# Gamair
Website that provide thousands of table games and let you to sort them by price, category, minimum age, number of players and popularity. The database is constantly updated thanks to a parser, developed to work with two website: uplay.it and giochinscatola.it
* Easy to use - User friendly
* Mobile Friendly - Visit the website with tablet, smartphone or pc
* Big community - See users reviews about products that you want to buy
* Newsletter updates - Get newsletter updates with new products
* Customer service - Ask any question and we will answer as fast as possible
## Getting Started
To download the project you have to clone this repository and you must be in the working team
```
git@bitbucket.org:gruppo_scrap/scraproject.git
```
### Prerequisites

You need at least PHP 7 to get SQLite working well.

### Installing

You have to move the folders into a web server or into Xampp htdoc folder (with xampp email sending could not work)

## Documentation

Parser: /data/JavaDoc
Website: /Website

## Libraries used
Parser
* [Simple java mail](#) - Java email sender framework
* [Common validator](#) - URL validation
* [Jsoup](#) - Java scrap library

Website
* [Bootstrap Framework](#) - For graphical components
* [Jquery](#) - For bootstrap
* [Ajax](#) - For many features

## Versioning
1.0 - 23/12/2016 22:50

## Authors

* **Lorenzo Merici**
* **Luca Sanini**
* **Alessio Carletto**
* **Alex Salvatico**
* **Elia Dutto**
* **Maicol Bianco**
* **Marco Tacinti**

## License

This project is licensed under free License
