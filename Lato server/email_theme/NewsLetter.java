import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.Mailer;
import org.simplejavamail.mailer.config.ServerConfig;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.TimerTask;

public class NewsLetter extends TimerTask{
    private DBmanager dbmanager;
    private LogManager logmanager;
    private Properties properties;

    public NewsLetter(DBmanager dbmanager, LogManager logmanager, Properties properties) {
        this.dbmanager = dbmanager;
        this.logmanager = logmanager;
        this.properties = properties;
    }

    public boolean send(List<String> subscribers, List<Prodotto> products){
        //todo controllo di NON reinvio degli stessi prodotti
        //todo quando premi nella mail sul nome di un prodotto deve reindirizzarti al sito web

        boolean ret = false;
        String temp;
        String html;
        EmailBuilder emailBuilder = new EmailBuilder();
        emailBuilder.from(properties.getProperty("nick"),properties.getProperty("smail"));

        for(String ele : subscribers){
            temp = ele.substring(0,ele.indexOf("@"));
            emailBuilder.to(temp,ele);
        }

        html = getHTML(products);

        if(html.isEmpty()){
            logmanager.error("generating html");
            return false;
        }
        emailBuilder.textHTML(html);
        emailBuilder.subject("GAMAIR - Ecco le nuove offerte");

        new Mailer(new ServerConfig(properties.getProperty("smtp_host"), 587, properties.getProperty("smtp_account"), properties.getProperty("smtp-pwd"))).sendMail(emailBuilder.build());
        System.out.println("Emails sent");
        return ret;
    }

    //overload se si vuole mandare una pagina viene chiamato dal metodo run(String html)
    public boolean send(List<String> subscribers, String html){
        //todo controllo di NON reinvio degli stessi prodotti
        //todo quando premi nella mail sul nome di un prodotto deve reindirizzarti al sito web

        boolean ret = false;
        String temp;
        EmailBuilder emailBuilder = new EmailBuilder();
        emailBuilder.from(properties.getProperty("nick"),properties.getProperty("smail"));

        for(String ele : subscribers){
            temp = ele.substring(0,ele.indexOf("@"));
            emailBuilder.to(temp,ele);
        }

        if(html.isEmpty()){
            logmanager.error("The html is null");
            return false;
        }
        emailBuilder.textHTML(html);
        emailBuilder.subject("GAMAIR - Ecco le nuove offerte");

        new Mailer(new ServerConfig(properties.getProperty("smtp_host"), 587, properties.getProperty("smtp_account"), properties.getProperty("smtp-pwd"))).sendMail(emailBuilder.build());
        System.out.println("Emails sent");
        return ret;
    }

    public void run() {
        System.out.println("Newsletter started....");
        List<String> emails = dbmanager.GetSubscribers();
        List<Prodotto> productsTosend = dbmanager.GetNewAddedProducts(Integer.parseInt(properties.getProperty("products_to_send")));
        send(emails,productsTosend);
    }

    public void run(String html) {
        System.out.println("Newsletter started....");
        List<String> emails = dbmanager.GetSubscribers();
        send(emails,html);
    }


    public String getHTML(List<Prodotto> products){
        String html = "";

        try {
            html = new String(Files.readAllBytes(Paths.get("./email_theme/email.html")));
        } catch (IOException e) {
            logmanager.error("reading file with html template");
        }
        for(int i = 0; i < products.size(); i++){
            int desc_size = products.get(i).getDescrizione().length();
            if(desc_size > 120)
                desc_size = 120;
            //link del prodotto per ora e temporaneo
            html = html.replace("#image_product_"+ i,products.get(i).getImmagine()).replace("#descr_product_" + i,products.get(i).getDescrizione().substring(0,desc_size) + "...").replace("#name_product_" + i,products.get(i).getNome()).replace("#link_product_" + i,link);
        }

        return html;
    }
}
