/**
 * Created by Lorenzo Merici on 01/11/2016.
 */
import java.io.*;
import java.time.LocalDateTime;
import java.util.Properties;

class FilesManager{
    private File logFile = new File("LOG.txt");
    private File csvFile = new File("Products.csv");
    private FileOutputStream oFile;
    private FileOutputStream csvOutput;

    private Properties prop;

    void addProduct(String desc){
	if(oFile != null){
	    try{
            csvOutput.write(desc.getBytes());
            csvOutput.write('\n');
	    }catch(IOException e){
		    System.out.println("Error during csv insert");
	    }	
	}
    }

    void logWrite(String message){
        if(oFile != null){
            try {
                oFile.write((LocalDateTime.now() + "  -  " + message).getBytes());
                oFile.write('\n');

            } catch (IOException e) {
                System.out.println("Error during logWrite");
            }
        }

        System.out.println(message);
    }

    void error(String error){
        logWrite("ERROR: " + error);
    }

    void clearFile(){
        try{
            if(!csvFile.exists())
                csvFile.createNewFile();
            csvOutput = new FileOutputStream(csvFile,false);

        }catch (IOException e){
            System.out.println("Errore durante la creazione del file CSV temporaneo");
            csvFile = null;
        }
    }

    FilesManager(Properties properties){
        prop = properties;
	
	//creo il file CSV temportano

	try{
        if(!csvFile.exists())
	        csvFile.createNewFile();
	    csvOutput = new FileOutputStream(csvFile,false);

	}catch (IOException e){
	    System.out.println("Errore durante la creazione del file CSV temporaneo");
	    csvFile = null;	
	}    

	//creo il file di lOG

    if(Integer.parseInt(properties.getProperty("create_log_file")) != 0){
        try {
            logFile.createNewFile(); // if file already exists will do nothing
            oFile = new FileOutputStream(logFile, false);
        } catch (IOException e) {
            System.out.println("Errore durante la creazione del file di LOG.");
        }
    }else
        oFile = null;
    }

    BufferedReader getFileReader(){
        try {
            return new BufferedReader(new FileReader(csvFile));
        } catch (FileNotFoundException e) {
            return null;
        }
    }

}
