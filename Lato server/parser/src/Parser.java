/**
 * Created by Lorenzo on 19/10/2016.

 */


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class Parser {

    public static void main(String[] args){
        if(args.length != 1){
            System.out.println("Something wrong, I need a configuration file. \n Please give me a .properties file");
            return;
        }
        FilesManager filesManager;
        DBmanager dbmanager;
        Properties properties = new Properties();
        InputStream propFile = null;
        /*
        Carico il file di configurazione
         */
        try {
            propFile = new FileInputStream("config.properties");
        } catch (FileNotFoundException e) {
            System.out.println("Properties file not found");
            return;
        }

        try {
            properties.load(propFile);
        } catch (IOException | IllegalArgumentException e) {
            System.out.println("File error");
            return;
        }
        System.out.println("Correct properties loading");

        /**
         * Set up a DataBase manager and a LogManager
         */
        filesManager = new FilesManager(properties);
        dbmanager = new DBmanager(filesManager,properties);

        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, Integer.parseInt(properties.getProperty("ora_avvio_servizi")));
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);

        /**
         * Set up the timer. The thread will start every day at two o'clock
         */
        Timer timer = new Timer();
        /*
            imposto lo spider. Il thread viene eseguito ogni tot tempo, come precisato nel file di configurazione
         */
        timer.schedule(new Spider(filesManager, dbmanager,properties), today.getTime(), TimeUnit.MILLISECONDS.convert(Integer.parseInt(properties.getProperty("frequenza_parser")), TimeUnit.DAYS));

        /*
            imposto la newsletter.

            SICURAMENTE UN PESSIMO APPROCCIO
            Includerere la newsletter nel parser è molto limitante,
            ma non ho tempo per staccarla e inserirla a parte.

            Ogni x giorni viene inviata a tutti gli iscritti con gli ultimi 4 prodotti aggiunti
         */
        timer.schedule(new NewsLetter(dbmanager,filesManager,properties),today.getTime(),TimeUnit.MILLISECONDS.convert(Integer.parseInt(properties.getProperty("frequenza_newsletter")),TimeUnit.DAYS));

    }

}


