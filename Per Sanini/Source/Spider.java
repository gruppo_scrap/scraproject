import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.sqlite.core.DB;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.TimerTask;

class Spider extends TimerTask {
    private final String UPLAY_PRODUCTS_LIST_URL = "https://www.uplay.it/index.php?page=giochi&cat=giochi_da_tavolo&pg=";
    private final String GIS_PRODUCTS_LIST_URL = "http://www.giochinscatola.it/en/13-boardgames?p=";
    private final Integer MAX_TRIES_CONNECTION_URL = 6;

    private FilesManager filesManager;
    private DBmanager dbmanager;
    private Properties properties;
    private int productsCounter = 0;

    Spider(FilesManager filesManager, DBmanager dBmanager, Properties properties){
        this.filesManager = filesManager;
        this.dbmanager = dBmanager;
        this.properties = properties;
    }

    /**
     * This method receive an url and try to connect MAX_TRIES_CONNECTION_URL times;
     * @param url url to connect
     * @return Document
     */
    private Document getDoc(String url){
        Document doc;
        int count = 0;

        //Genero un prodotto

        while(true) {
            try {
                doc = Jsoup.connect(url).followRedirects(false).get();
                break;
            } catch (IOException e) {
                // handle exception
                if (++count == MAX_TRIES_CONNECTION_URL){
                    filesManager.error("something wrong in connection URL: " + url + "\n" + e.getMessage());
                    return null;
                }
            }
        }

        return doc;
    }

    /**
     * Riceve un parametro del tipo URL del sito web uplay.it
     * Per ognuno di essi genera un prodotto effettuando delle select all'interno
     * del Dom della pagina.
     * @param url the URL of a product
     * @return true if correctly parsed
     */

    private boolean parseProductPageUPLAY(String url){

        Document doc;
        Elements stuff;
        Element ele;

        //Genero un prodotto
        Prodotto prodotto = new Prodotto(filesManager);

        if((doc = getDoc(url)) == null)
            return false;

        //prendo il titolo
        stuff= doc.select(".contentframe tr h1");

        prodotto.setNome(stuff.first().text());

        //prendo il prezzo
        stuff.clear();
        stuff= doc.select(".contentframe table tr td span");
        /*
            Alcuni prodotti hanno il prezzo scontato. Controllo che non
            siano colorati di rosso. Se lo sono, prendo il prezzo successivo, quello scontato.
         */
        if(!stuff.first().attr("style").contains("#F00"))
            prodotto.setPrezzo(stuff.first().text());
        else
            prodotto.setPrezzo(stuff.get(1).text());

        //prendo immagine
        stuff= doc.select("#homefoto img");
        prodotto.setImmagine("https://www.uplay.it/" + stuff.first().attr("src"));

        //prendo informazioni
        stuff= doc.select("#infobox strong");

        //prendo numero giocatori
        ele = stuff.get(1);
        prodotto.setMaxMinGiocatori(ele.text());

        //prendo eta minima
        ele = stuff.get(2);
        prodotto.setEtaMinima(ele.text());

        //prendo anno produzione
        ele = stuff.get(3);
        prodotto.setAnnoProduzione(ele.text());


        // prendo descrizione
        stuff= doc.select("#scrizionediv");
        prodotto.setDescrizione(stuff.first().text());

        //inserisco l'url
        prodotto.setURL(url);

        //prendo immagini alternative
        prodotto.setAlternativeImages(doc.select("#scrizionediv img"));

        //prendo categorie
        stuff= doc.select("#infobox tr td");
        /*
            - Seleziono tutti gli elementi del box informazioni
            - Ciclo su di essi, se sono uguali all'informazione che cerco allora l'elemento successivo
              della lista va nel prodotto.
         */

        for(int i = 0; i < stuff.size(); i++){
            if(stuff.get(i).text().toLowerCase().equals("categorie")){
                prodotto.setCategorie(stuff.get(i+1).text());
                continue;
            }
            if(stuff.get(i).text().toLowerCase().equals("casa editrice")){
                prodotto.setPublisher(stuff.get(i+1).text());
                continue;
            }
            if(stuff.get(i).text().toLowerCase().equals("autori")){
                prodotto.setAutori(stuff.get(i+1).text());
                break;
            }
        }

        //Aggiungo il prodotto al file CSV
        if(prodotto.isThisProductUsable()){
            filesManager.addProduct(prodotto.getCSVRecord());
            filesManager.logWrite("CORRECTLY SCRAPED: " + url);
            return true;
        }
        return false;
    }

    /**
     * Riceve un parametro del tipo URL del sito web giochinscatola.it
     * Per ognuno di essi genera un prodotto effettuando delle select all'interno
     * del Dom della pagina.
     * @param url an product page url
     * @return true if it works, false if not. Now it is not managed because i manage errors into setters
     */
    private boolean parseProductPageGIS(String url){

        Document doc;
        Elements stuff;
        int cont;
        //Genero un prodotto
        Prodotto prodotto = new Prodotto(filesManager);

        if((doc = getDoc(url)) == null)
            return false;

        //prendo il prezzo

        stuff= doc.select("#tablecombz-table .price");
        if(stuff.isEmpty()){
            filesManager.error("Product without price. Unusable");
            return false;
        }
        prodotto.setPrezzo(stuff.first().text());


        //prendo il titolo
        stuff= doc.select(".heading");
        prodotto.setNome(stuff.first().text());

        //prendo immagine
        stuff.clear();
        stuff= doc.select(".bigpic_item img");
        if(stuff.isEmpty()){
            if(Integer.parseInt(properties.getProperty("psi")) == 0){
                //interrompo
                filesManager.error("Product without image -> parser interrupted");
                return false;
            }
        }else{
            prodotto.setImmagine(stuff.first().attr("src"));
        }

        //prendo descrizione
        stuff.clear();
        stuff= doc.select(".rte .pa_content");
        if(!stuff.isEmpty())
            prodotto.setDescrizione(stuff.first().text());
        else{
            if(Integer.parseInt(properties.getProperty("psd")) == 0){
                //interrompo
                filesManager.error("Product without description -> parser interrupted");
                return false;
            }
        }

        //setto URL
        prodotto.setURL(url);
        //prendo informazioni
        stuff.clear();

        //prendo numero giocatori min
        stuff = doc.select(".table-data-sheet tr");

        if(stuff.size() <= 0){
            filesManager.error("This product has not data-sheet: " + url + "--> parser interrupted");
            return false;
        }


        stuff = doc.select(".table-data-sheet td");


        for(int i = 0; i < stuff.size(); i++){
            String elementName = stuff.get(i).text().trim().toLowerCase();
            if(elementName.equals("designer")){
                prodotto.setAutore(stuff.get(i+1).text());
                i++;
                continue;
            }
            if(elementName.equals("suggested ages")){
                prodotto.setEtaMinima(stuff.get(i+1).text());
                i++;
                continue;
            }
            if(elementName.equals("min. # of players")){
                prodotto.setMingiocatori(stuff.get(i+1).text());
                i++;
                continue;
            }
            if(elementName.equals("max # of players")){
                prodotto.setMaxgiocatori(stuff.get(i+1).text());
                i++;
                continue;
            }
            if(elementName.equals("famiglia gioco")){
                prodotto.setCategoria(stuff.get(i+1).text());
                i++;
                continue;
            }
            if(elementName.equals("year published")){
                prodotto.setAnnoProduzione(stuff.get(i+1).text());
                i++;
            }

        }

        //Parsifico la casa editrice
        stuff = doc.select("#tablecombz-table thead tr th");
        for(cont = 0; ((cont < stuff.size()) && !stuff.get(cont).text().trim().toLowerCase().equals("publisher")); cont++) { }

        stuff = doc.select("#tablecombz-table tbody tr td");
        if(cont < stuff.size())
            prodotto.setPublisher(stuff.get(cont).text());



        //Aggiungo il prodotto al file CSV
        if(prodotto.isThisProductUsable()){
            filesManager.addProduct(prodotto.getCSVRecord());
            filesManager.logWrite("CORRECTLY SCRAPED: " + url);
            return true;
        }

        return false;
    }

    /**
     * Riceve l'url di una pagina contenente un elenco di prodotti. Il metodo concatena al termine della stringa un contatore
     * che viene incrementato ogni volta per ciclare ogni pagina del sito web con un elenco di prodotti.
     * Per ogni pagina, grazie alla libreria JSoup (MIT License), grazie ad una stringa diversa in base alla
     * struttura del sito web, ottengo tutti gli elementi HTML contentenenti link a prodotti nell'eleneco.
     * Appena ottengo un URL,lo verifico grazie ad un URLtester, chiamo i metodi atti a parsificarlo e generare un prodotto
     * @param url_products_list Un url della pagina con l'elenco dei prodotti senza il contatore finale es 'pag='
     * @return true if done, false if error
     */
    private boolean parse_products_list_page(String url_products_list){
        String detectedUrl = "firstvalue";
        Document doc;
        Element temp = null;
        Elements stuff;
        UrlValidator URLtester = new UrlValidator();
        String treeString = " ";


        int pageCounter = 0;

        if(url_products_list.equals(GIS_PRODUCTS_LIST_URL))
           pageCounter = 1;

        if((doc = getDoc(url_products_list + Integer.toString(pageCounter))) == null)
            return false;

        if(url_products_list.equals(UPLAY_PRODUCTS_LIST_URL))
            treeString = ".listagiochi tr td a";
        else
            if(url_products_list.equals(GIS_PRODUCTS_LIST_URL))
                treeString = ".product_img_link";

        stuff= doc.select(treeString);
        /*
         * Loop each element of each page
         */
        while(!stuff.isEmpty()){
            for (Element aStuff : stuff) {
                temp = aStuff;
                if (detectedUrl.equals(temp.attr("href"))) {
                    continue;
                }
                detectedUrl = temp.attr("href");
                if (URLtester.isValid(detectedUrl)) {
                    productsCounter++;
                    System.out.print("Products scraped [" + productsCounter + "]\r");
                    if (url_products_list.equals(UPLAY_PRODUCTS_LIST_URL))
                        parseProductPageUPLAY(detectedUrl);
                    else if (url_products_list.equals(GIS_PRODUCTS_LIST_URL))
                        parseProductPageGIS(detectedUrl);

                }
            }
            pageCounter ++;

            if((doc = getDoc(url_products_list + Integer.toString(pageCounter))) == null)
                return false;

            stuff.clear();
            stuff= doc.select(treeString);
        }

        return true;
    }
    public void run() {


        System.out.println("Starting spider...\n\n");
        filesManager.clearFile();
        parse_products_list_page(GIS_PRODUCTS_LIST_URL);
        parse_products_list_page(UPLAY_PRODUCTS_LIST_URL);
        System.out.println("Starting data saver...");

        dbmanager.insertData();
        System.out.println("Done");


    }


}
