import com.sun.org.apache.regexp.internal.RE;
import com.sun.org.apache.xml.internal.security.algorithms.implementations.IntegrityHmac;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Lorenzo Merici on 01/11/2016.
 */

/**
 * Questa classe viene istanziata ogni volta che un prodotto viene
 * estratto dal parser.
 * I metodi sono tutti packaged-local, di default senza una visibilità dichiarata
 */
public class Prodotto {
    private final String CSV_SEPARATOR = ";";
    private final String CSV_REPLACED_VALUE = "/&#/";
    private final String N_REPLACED_VALUE = "/*#/";

    private final int MAX_DESC_SIZE = 1000;
    private FilesManager filesManager;
    private String nome;
    private String immagine;
    private int prezzo;
    private int mineta;
    private int annoProduzione;
    private int productID;
    private String publisher;



    /*
         * When i don't have data, -1 will be stored into DB.
         */
    private int mingiocatori = -1;
    private int maxgiocatori = -1;
    private String URL;
    private String descrizione;
    private List<String> autori = new ArrayList<>(0);
    private List<String> categorie = new ArrayList<>(0);
    private List<String> alternativeImages = new ArrayList<>(0);
    private boolean isThisProductUsable = true;

    Prodotto(FilesManager filesManager) {
        this.filesManager = filesManager;
    }

    Prodotto() {

    }

    /**
     * Costruisce un prodotto ricevendo una stringa.
     * Serve a generare un prodottoa partire da una linea del file CSV
     * Queste stringhe vengono generate dal metodo getCSVrecord();
     * Sostituisce inoltre i caratteri speciali con quelli originali.
     *
     * @param recordCSV linea di un file CSV
     */
    Prodotto(String recordCSV){

        int contAutori, contCategorie, contImages;
        String[] data = recordCSV.split(CSV_SEPARATOR);
        nome = data[0].replace(CSV_REPLACED_VALUE,CSV_SEPARATOR).replace(N_REPLACED_VALUE,"\n");
        immagine = data[1].replace(CSV_REPLACED_VALUE,CSV_SEPARATOR).replace(N_REPLACED_VALUE,"\n");
        prezzo = Integer.parseInt(data[2]);
        mineta = Integer.parseInt(data[3]);
        annoProduzione = Integer.parseInt(data[4]);
        mingiocatori = Integer.parseInt(data[5]);
        maxgiocatori = Integer.parseInt(data[6]);
        URL = data[7].replace(CSV_REPLACED_VALUE,CSV_SEPARATOR).replace(N_REPLACED_VALUE,"\n");
        descrizione = data[8].replace(CSV_REPLACED_VALUE,CSV_SEPARATOR).replace(N_REPLACED_VALUE,"\n");
        publisher = data[9].replace(CSV_REPLACED_VALUE,CSV_SEPARATOR).replace(N_REPLACED_VALUE,"\n");

        contAutori = Integer.parseInt(data[10]);
        contCategorie = Integer.parseInt(data[11]);
        contImages = Integer.parseInt(data[12]);
        autori.addAll(Arrays.asList(data).subList(11, 11 + contAutori));
        categorie.addAll(Arrays.asList(data).subList(11 + contAutori, 11 + contAutori + contCategorie));
        alternativeImages.addAll(Arrays.asList(data).subList(11 + contAutori + contCategorie,11 + contAutori + contCategorie + contImages));

    }

    public String getNome() {
        if(nome.isEmpty())
            return "Nome non disponibile";
        return nome;
    }
    /**
     * il metodo set Nome riceve una stringa come parametro, il nome del prodotto.
     * Prima di utilizzarla effettua una trim per eliminare eccessivi spazi.
     * In seguito viene testata. Nel caso fosse vuota il prodotto viene settato come
     * inutilizzabile e non verrà caricato nel database.
     * Essa funziona per ogni sito web.
     * Non ritorna nulla poichè siccome rarissime volte il prodotto non ha un nome,
     * sarebbe meno efficente per ogni prodotto testare un evventuale boolean di ritorno.
     * @param nome il nome del prodotto
     *
     */

    public void setNome(String nome) {
        nome = nome.trim();
        if(nome.isEmpty()){
            filesManager.error("I'm without name!");
            isThisProductUsable = false;
        }else
            this.nome = nome;
    }

    /**
     *
     * @return il path dell'immagine prodotto
     */
    public String getImmagine() {
        return immagine;
    }

    /**
     *  Setta il percorso dell'immagine di un prodotto
     *  Effettua una trim controlla che il valore esista.
     *  Non viene testato come URL poichè risultato di una .value su un attributo src.
     *  Se non vi è un'immagine il prodotto è inutilizzabile
     * @param immagine, l'URL dell'immagine
     */
    public void setImmagine(String immagine) {
        immagine = immagine.trim();
        if(immagine.isEmpty()){
            filesManager.error("I'm without image!");
            isThisProductUsable = false;
        }
        else
            this.immagine = immagine;
    }

    /**
     *
     * @return L'URL del prodotto
     */
    public String getURL() {
        return URL;
    }

    /**
     * Riceve e setta l'URL del prodotto
     * @param URL l'url del prodotto
     */
    public void setURL(String URL) {
        URL = URL.trim();
        this.URL = URL;
    }
    /**
     * @return La descrizione del prodotto
     */
    public String getDescrizione() {
        if(descrizione == null){
            return "Descrizione non disponibile";
        }
        return descrizione;
    }

    /**
     * Setta la descrizione di un prodotto.
     * Controlla che la lunghezza della descrizione non superi
     * il limite di MAX_DESC_SIZE, in caso contrario la tronca e aggiunge tre punti in coda
     * todo gestione interna delle opzioni
     *
     * @param descrizione una descrizione del prodotto
     */
    public void setDescrizione(String descrizione) {
        if(descrizione == null)
            return;
        descrizione = descrizione.trim();
        if(descrizione.length() > MAX_DESC_SIZE)
            this.descrizione = descrizione.substring(0,MAX_DESC_SIZE) + "...";
        this.descrizione = descrizione;
    }

    /**
     *
     * @return il prezzo del prodotto, come intero.
     */
    public int getPrezzo() {
        return prezzo;
    }

    /**
     * Setta il prezzo di un prodotto. Inizialmente al valore del parametro vengono
     * eliminati spazi aggiunti, i caratteri maiuscoli e minuscoli, il simbolo dell'Euro
     * e la punteggiatura. Il prodotto viene quindi parsificato e settato.
     * Si può generare una NumberFormatException. gestita definendo inutilizzabile il prodotto.
     * Funzione su tutti i siti utilizzati
     * @param prezzo
     */
    public void setPrezzo(String prezzo) {
        prezzo = prezzo.trim().replaceAll("[a-zA-Z:€]","").trim();
        prezzo = prezzo.replace(",","");

        try {
            this.prezzo = Integer.parseInt(prezzo);
        } catch (NumberFormatException e) {
            filesManager.error("I'm without price!");
            isThisProductUsable = false;
        }

    }

    /**
     * #UPLAY.IT website
     *  Riceve una stringa formattata nel tipo 'min - max' giocatori,
     *  come nel sito Uplay.it
     *  Iniziamente verifico che la proprietà non sia settata come N.D., non disponibile.
     *  Viene eseguita una split per suddividere i valori e in seguito parsificati.
     *  Talvolta succede che vi sia soltanto un valore minimo di giocatori.
     *  Gli altri vengono settati a -1
     * @param maxmin stringa con 'max - min'
     */
    public void setMaxMinGiocatori(String maxmin){
        maxmin = maxmin.trim();
        if(maxmin.equals("N.D.") || maxmin.isEmpty())
            return;

        String[] splittedplayers;
        splittedplayers = maxmin.split("-");
        try {
            if (splittedplayers.length == 1) {
                mingiocatori = Integer.parseInt(splittedplayers[0]);
                maxgiocatori = -1;
            } else {
                if (splittedplayers.length == 2) {
                    mingiocatori = Integer.parseInt(splittedplayers[0]);
                    maxgiocatori = Integer.parseInt(splittedplayers[1]);
                } else {
                    mingiocatori = -1;
                    maxgiocatori = -1;
                }
            }
        }catch (NumberFormatException e){
            mingiocatori = -1;
            maxgiocatori = -1;
        }
    }

    /**
     * Setta l'età minima consigliata.
     * Riceve una stringa che, se disponibile, parsifica. Se non presente
     * setta a -1 il valore
     * @param eta
     */
    public void setEtaMinima(String eta){
        if(!eta.equals("N.D."))
            try{
                mineta = Integer.parseInt(eta.replace("+",""));
            }catch(NumberFormatException e){
                mineta = -1;
            }
        else
            mineta = -1;
    }

    /**
     *
     * @return Lista di autori
     */
    public List<String> getAutore() {
        return autori;
    }

    /**
     *
     * @return lista di categorie
     */
    public List <String> getCategorie(){
        return categorie;
    }

    /**
     * Questo metodo consente di sapere se il prodotto è utilizzabile
     * o se manca di qualche proprietà necessaria.
     * @return boolen;
     */
    public boolean isThisProductUsable() {
        return isThisProductUsable;
    }

    /**
     * Riceve e setta l'autore del gioco.
     * Il metodo può essere chiamato n volte poichè n possono essere gli autori.
     * Vi è una lista a cui viene aggiunto ogni autore.
     * @param autore Stringa con il nome di un autore
     */
    public void  setAutore(String autore) {
        autore = autore.trim();

        if(!autore.isEmpty() && autore.toLowerCase().equals("(uncredited)"))
            this.autori.add(autore);
    }

    /**
     * Aggiunge una categoria alla lista di categorie
     * @param categoria Nome della categoria
     */
    public void  setCategoria(String categoria) {
        categoria = categoria.trim();

        if(!categoria.isEmpty())
            this.categorie.add(categoria);
    }

    /** #UPLAY
     * Riceve una stringa con le varie categorie del prodotto separate da un trattino.
     * Funziona con il sito uplay.it
     *
     * @param categories
     */
    public void setCategorie(String categories){
        categories = categories.trim();
        if(categories.isEmpty())
            return;
        String[] tempCategories;
        tempCategories = categories.split("-");
        for(String cat :  tempCategories){
            cat = cat.trim();
            if(!cat.isEmpty())
                this.categorie.add(cat);
        }
    }

    /**
     * #Uplay.it
     * Riceve e setta un elenco di autori.
     * Essi devono essere separati da un trattino.
     * Separati con una split, vengono aggiunti ad una lista di autori.
     *
     * @param autori stringa con gli autori 'a - b - c - ...'
     */
    public void setAutori(String autori){
        autori = autori.trim();
        if(autori.isEmpty())
            return;
        String[] tempAutori;
        tempAutori = autori.split("-");
        for(String author :  tempAutori){
            author = author.trim();
            if(!author.isEmpty())
                this.autori.add(author);
        }
    }

    /**
     *
     * @return Eta minima per quel prodotto
     */
    public int getMineta() {

        return mineta;
    }

    /**
     * Ogni prodottp deve avere un immagine primaria, ma può non averne altre.
     * @return un vettore di immagini secondarie
     */
    public List<String> getAlternativeImages() {
        return alternativeImages;
    }

    /**
     * Riceve un' insieme di elementi, quelli contenenti le immagini secondarie del prodotto
     * per ogni elemento si risale al percordo dell'immagine e viene creato un vettore di immagini
     *
     * @param stuff collection of Elements
     */
    public void setAlternativeImages(Elements stuff){
        for(int i = 0; i < stuff.size(); i++){
            alternativeImages.add(stuff.get(i).attr("src"));
        }
    }

    /**
     *
     * @return integer, anno di produzione
     */
    public int getAnnoProduzione() {
        return annoProduzione;
    }

    /**
     * Riceve e setta l'anno di produzione di un prodotto.
     * Prima ne testa la disponibilità
     * @param annoProduzione integer
     */
    public void setAnnoProduzione(String annoProduzione) {
        if(!annoProduzione.equals("N.D."))
            this.annoProduzione = Integer.parseInt(annoProduzione.trim());
        else
            this.annoProduzione = -1;
    }

    /**
     *
     * @return numero massimo di giocatori
     */
    public int getMaxgiocatori() {
        return maxgiocatori;
    }
    /**
     * @return numero minimo di giocatori
     */
    public int getMingiocatori() {
        return mingiocatori;
    }

    /**
     * #GIS
     * Riceve una stringa che viene parsificata come intero e settata come
     * numero minimo di giocatori
     * funziona con giochinscatola.it
     * @param mingiocatori numero minimo di giocatori
     */
    public void setMingiocatori(String mingiocatori) {
        if(mingiocatori.isEmpty()){
            return;
        }
        this.mingiocatori = Integer.parseInt(mingiocatori);
    }

    /**
     * Setta il numero massimo di giocatori, se presente
     * @param maxgiocatori integer
     */
    public void setMaxgiocatori(String maxgiocatori) {
        if(maxgiocatori.isEmpty()){
            return;
        }
        this.maxgiocatori = Integer.parseInt(maxgiocatori);
    }
    public int getProductID() {
        return productID;
    }

    /**
     * Restituisce la casa editrice
     * @return casa editrice
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * riceve una casa editrice e la setta nel prodotto.
     * @param publisher casa editrice
     */
    public void setPublisher(String publisher) {
        if(!publisher.isEmpty() && !publisher.equals("N.D."))
            this.publisher = publisher.trim();
        else
            this.publisher = "";
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    @Override
    public String toString() {
        return "Prodotto{" +
                "nome='" + nome + '\'' +
                ", immagine='" + immagine + '\'' +
                ", prezzo=" + prezzo +
                ", mineta=" + mineta +
                ", annoProduzione=" + annoProduzione +
                ", publisher='" + publisher + '\'' +
                ", mingiocatori=" + mingiocatori +
                ", maxgiocatori=" + maxgiocatori +
                ", URL='" + URL + '\'' +
                ", descrizione='" + descrizione + '\'' +
                ", autori=" + autori +
                ", categorie=" + categorie +
                ", alternativeImages=" + alternativeImages +
                '}';
    }

    /**
     * Genera una stringa contenente i dati del prodotto, saparati dal carattere definito in CSV_SEPARATOR.
     * Formattazione:
     *  1 - nome
     *  2 - immagine
     *  3 - prezzo
     *  4 - Età minima consigliata
     *  5 - Anno di produzione
     *  6 - Numero minimo di giocatori
     *  7 - Numero massimo di giocatori
     *  8 - Url del prodotto
     *  9 - Descrizione
     *  10 - Casa editrice
     *  11 - X, Numero di designers che hanno lavorato al prodotto
     *  12 - Y, Numero di Categorie del prodotto
     *  13 - Z, Numero di ImmaginiAlternative del prodotto
     *
     *  14 -> X + Y + Z  autori, categorie e immagini prodotto
     * @return
     */
    public String getCSVRecord(){
        StringBuilder oneLine = new StringBuilder();
        /*
         * Sostituisco il carattere di separazione nei CSV e il carattere \n con altri che non
         * interferiscano
         */
        nome = nome.replace(CSV_SEPARATOR, CSV_REPLACED_VALUE).replace("\n",N_REPLACED_VALUE);
        immagine = immagine.replace(CSV_SEPARATOR, CSV_REPLACED_VALUE).replace("\n",N_REPLACED_VALUE);
        URL = URL.replace(CSV_SEPARATOR, CSV_REPLACED_VALUE).replace("\n",N_REPLACED_VALUE);
        descrizione = descrizione.replace(CSV_SEPARATOR, CSV_REPLACED_VALUE).replace("\n",N_REPLACED_VALUE);
        publisher = publisher == null ? "" : publisher.replace(CSV_SEPARATOR, CSV_REPLACED_VALUE).replace("\n",N_REPLACED_VALUE);
        for(int i = 0; i < autori.size(); i++)
            autori.set(i,autori.get(i).replace(CSV_SEPARATOR, CSV_REPLACED_VALUE).replace("\n",N_REPLACED_VALUE));
        for(int i = 0; i < categorie.size(); i ++)
            categorie.set(i,categorie.get(i).replace(CSV_SEPARATOR, CSV_REPLACED_VALUE).replace("\n",N_REPLACED_VALUE));
        for(int i = 0; i < alternativeImages.size(); i ++){
            alternativeImages.set(i,alternativeImages.get(i).replace(CSV_SEPARATOR, CSV_REPLACED_VALUE).replace("\n",N_REPLACED_VALUE));
        }
        /*
           NON CAMBIARE L'ORDINE. todo cerco best praticse
         */
        oneLine.append(nome.isEmpty() ? "" : nome);
        oneLine.append(CSV_SEPARATOR);
        oneLine.append(immagine.isEmpty() ? "" : immagine);
        oneLine.append(CSV_SEPARATOR);
        oneLine.append(prezzo);
        oneLine.append(CSV_SEPARATOR);
        oneLine.append(mineta);
        oneLine.append(CSV_SEPARATOR);
        oneLine.append(annoProduzione);
        oneLine.append(CSV_SEPARATOR);
        oneLine.append(mingiocatori);
        oneLine.append(CSV_SEPARATOR);
        oneLine.append(maxgiocatori);
        oneLine.append(CSV_SEPARATOR);
        oneLine.append(URL);
        oneLine.append(CSV_SEPARATOR);
        oneLine.append(descrizione.isEmpty() ? "" : descrizione);
        oneLine.append(CSV_SEPARATOR);
        oneLine.append(publisher.isEmpty() ? "" : publisher);
        oneLine.append(CSV_SEPARATOR);
        oneLine.append(autori.size());
        oneLine.append(CSV_SEPARATOR);
        oneLine.append(categorie.size());
        oneLine.append(CSV_SEPARATOR);
        oneLine.append(alternativeImages.size());
        oneLine.append(CSV_SEPARATOR);

        for (String autore: autori) {
            oneLine.append(autore);
            oneLine.append(CSV_SEPARATOR);
        }
        for(String cat: categorie){
            oneLine.append(cat);
            oneLine.append(CSV_SEPARATOR);
        }
        for(String image : alternativeImages){
            oneLine.append(image);
            oneLine.append(CSV_SEPARATOR);
        }

        return oneLine.toString();
    }
}
