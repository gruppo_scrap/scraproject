import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.Mailer;
import org.simplejavamail.mailer.config.ServerConfig;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.TimerTask;

/**
 *
 */
public class NewsLetter extends TimerTask{
    private final int CHARS_IN_PREVIEW = 120;
    private DBmanager dbmanager;
    private FilesManager filesManager;
    private Properties properties;

    public NewsLetter(DBmanager dbmanager, FilesManager filesManager, Properties properties) {
        this.dbmanager = dbmanager;
        this.filesManager = filesManager;
        this.properties = properties;
    }

    /**
     * Dato un elenco di iscritti e di prodotti da inviare, crea un file da inviare
     * grazie al metodo getHTML();
     * In seguito
     * @param subscribers Lista di iscritti
     * @param products Lista di prodotti
     * @return
     */
    public boolean send(List<String> subscribers, List<Prodotto> products){

        boolean ret = false;
        String temp;
        String html;

        EmailBuilder emailBuilder = new EmailBuilder();
        emailBuilder.from(properties.getProperty("nick"),properties.getProperty("smail"));

        for(String ele : subscribers){
            temp = ele.substring(0,ele.indexOf("@"));
            emailBuilder.to(temp,ele);
        }

        html = getHTML(products);

        if(html.isEmpty()){
            filesManager.error("generating html");
            return false;
        }
        emailBuilder.textHTML(html);
        emailBuilder.subject(properties.getProperty("subject"));

        new Mailer(new ServerConfig(properties.getProperty("smtp_server"), Integer.parseInt(properties.getProperty("smtp_port")), properties.getProperty("smtp_account"), properties.getProperty("smtp_pwd"))).sendMail(emailBuilder.build());
        System.out.println("Emails sent");
        return ret;
    }


    public void run() {
        System.out.println("Newsletter started....");
        /*
         * ottengo un elenco di iscritti
         */
        List<String> emails = dbmanager.GetSubscribers();
        /*
         * Ricevo una lista di prodotti da inviare
         */
        List<Prodotto> productsTosend = dbmanager.GetNewAddedProducts(Integer.parseInt(properties.getProperty("products_to_send")));
        /*
        Invio le emails
         */
        send(emails,productsTosend);
    }

    /**
     * Riceve una lista di prodotti. In seguito carica un file .html
     * contenente il template della mail da inviare.
     * Molto semplicemente, il file template contiene dei segnaposto,
     * ovvero delle stringhe che cominciano con # e in seguito una stringa particolare per ogni elemento.
     * Per ogni prodotto sostituisco nel file le stringhe segnaposto con i valori del prodotto e genero
     * così l'HTML da inviare.
     *
     * @param products una lista di prodotti
     * @return Una stringa contenente l'HTML da inviare
     */
    public String getHTML(List<Prodotto> products){
        String html = "";

        try {
            html = new String(Files.readAllBytes(Paths.get("./email_theme/email.html")));
        } catch (IOException e) {
            filesManager.error("reading file with html template");
        }
        for(int i = 0; i < products.size(); i++){
            int desc_size = products.get(i).getDescrizione().length();
            if(desc_size > CHARS_IN_PREVIEW)
                desc_size = CHARS_IN_PREVIEW;
            html = html.replace("#image_product_"+ i,products.get(i).getImmagine()).replace("#descr_product_" + i,products.get(i).getDescrizione().substring(0,desc_size) + "...").replace("#name_product_" + i,products.get(i).getNome());
        }

        return html;
    }
}
