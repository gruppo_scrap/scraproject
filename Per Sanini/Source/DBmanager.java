import java.io.BufferedReader;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

class DBmanager{
    private FilesManager filesManager;
    private Properties properties;

    DBmanager(FilesManager filesManager, Properties prop)
    {
        properties = prop;
        this.filesManager = filesManager;
    }

    /**
     * Dato un Url, verifica se esso è presente nel DB.
     * Considero l'url una chiave candidata, anche se non utilizzabile come chiave primaria
     * a causa della sua lunghezza.
     *
     * @param url Url del prodotto
     * @return ID del prodotto se esiste, null se non vi sono prodotti con quell'Url
     */
    public Integer getProductID(String url){
        int ret = -1;
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = getDBConnection();
        String sql = "SELECT Product_ID FROM Products WHERE Product_Url ='" + url + "'";

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            this.filesManager.error("Error during getProductID query");
        }

        try {

            while(resultSet.next()){
                ret =  resultSet.getInt("Product_ID");
            }

        } catch (SQLException | NullPointerException e ) {
            e.printStackTrace();
            this.filesManager.error("During next(). Forse ResultSet non è stato inizializzato");
        }

        try {
            if (statement != null) {
                statement.close();
            }
            connection.close();
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }

        return ret == -1 ? null : ret;
    }

    /**
     * Salva i dati sul database. Riceve dal file manager un filereader,
     * e legge una riga per volta. Una volta generato il prodotto, lo salva.
     *
     * Avrei preferito un metodo che restituisse dal file manager una riga per volta,
     * ma le soluzioni a cui ho pensato non mi sembravano molto pulite.
     *
     * #PROBLEMA:
     * La mia intenzione era di eseguire una sola transazione per velocizzare le tempistiche
     * (Come anche suggerito dal docente di laboratorio di Informatica), ma dopo
     * circa 1000 prodotti SQLite restituisce un'eccezione SQLITE_BUSY. Non escludo che possa
     * essere un mio errore, ma non saprei come risolverlo.
     * Ho trovato un compromesso inserendo nel file di configurazione la voce "products_in_transaction",
     * ovvero il numero massimo di prodotti dopo il quale verrà eseguito il commit.
     * Inoltre non utilizzo i metodi proprio del JDBC di commit, rollback e setAutoCommit poichè
     * generano un eccezione "PreparedStatement has been finalized", che non so come gestire.
     * Probabilmente sbaglio qualcosa; su stackoverflow alcuni hanno avuto il mio problema,
     * e un utente ha segnalato un Bug di SQLite. Ho anche posto la domanda, ma nessuna
     * risposta esaustiva.
     *
     *
     * Sicuramente il metodo non è ottimizzato.
     * Ad esempio, il 99% delle UPDATE probabilmente sarà inutile, poichè pochissimi
     * sono i prodotti che vengono aggiornati. Pensavo ad un MD5 da aggiungere al
     * database e da confrontare per vedere se qualcosa è cambiato, ma non so
     * quanto possa velocizzare il processo.
     *
     * In ogni caso utilizzo una sola connessione al database per tutti i prodotti
     * e molti preparedStatement, più sicuri rispetto ai normali Statement.
     *
     *
     * @return true se
     */

    void insertData(){
        PreparedStatement stmt = null;
        ResultSet rs;
        Integer id_prodotto;
        Connection connection = getDBConnection();
        BufferedReader reader = filesManager.getFileReader();
        String line;
        Prodotto prodotto;

        int cont = 0;

        try {
            stmt = connection.prepareStatement("BEGIN");
            stmt.execute();

            while((line = reader.readLine()) != null) {
                cont ++;
                //genero un prodotto da una line del file CSV
                prodotto = new Prodotto(line);


                //controllo se il prodotto è nel DB.
                id_prodotto = getProductID(prodotto.getURL());
                int id_product_to_add;

                try {
                    if (id_prodotto == null) {
                        //devo aggiungere il prodotto

                        //ottengo l'ID
                        stmt = connection.prepareStatement("SELECT Hit FROM Products WHERE Product_ID = '-1'");
                        rs = stmt.executeQuery();

                        rs.next();
                        id_product_to_add = rs.getInt("Hit");
                        id_prodotto = id_product_to_add;

                        //incremento il prossimo ID da inserire
                        id_product_to_add++;
                        stmt = connection.prepareStatement("UPDATE Products SET Hit = " + id_product_to_add + " WHERE Product_ID = '-1'");
                        stmt.executeUpdate();
                        stmt = connection.prepareStatement("INSERT INTO Products (Product_ID,Name,Image_Url,Price,MinAge,Production_Year,MinPlayers,MaxPlayers,Product_Url,Description, AddDate) values (" + id_product_to_add + ",?, ?,?,?, ?,?,?, ?,?,?);");


                    /*d
                        Solo nel caso di un nuovo prodotto inserisco la data
                     */

                        LocalDateTime localDateTime = LocalDateTime.now();
                        stmt.setString(10, String.valueOf(localDateTime.getYear() + localDateTime.getDayOfYear()));


                    } else {
                        stmt = connection.prepareStatement("UPDATE Products SET Name = ?, Image_Url = ?,Price = ?, MinAge = ?, Production_Year = ?,MinPlayers = ?,MaxPlayers = ?, Product_Url = ?, Description = ? WHERE Product_Url = '" + prodotto.getURL() + "';");
                    }

                    //passo al PreparedStatement i dati da inserire
                    stmt.setString(1, prodotto.getNome());
                    stmt.setString(2, prodotto.getImmagine());
                    stmt.setInt(3, prodotto.getPrezzo());
                    stmt.setInt(4, prodotto.getMineta());
                    stmt.setInt(5, prodotto.getAnnoProduzione());
                    stmt.setInt(6, prodotto.getMingiocatori());
                    stmt.setInt(7, prodotto.getMaxgiocatori());
                    stmt.setString(8, prodotto.getURL());
                    stmt.setString(9, prodotto.getDescrizione());
                    stmt.executeUpdate();

                } catch (SQLException e) {
                    e.printStackTrace();
                    filesManager.error("Error during insert query");
                    rollBack(connection);
                    return;
                }
            /*
                Inserisco le immagini alternative

             */
                try {
                    saveAlternativesImages(prodotto.getAlternativeImages(),connection,id_prodotto);
                }catch (SQLException e){
                    filesManager.error("Saving alternative images");
                }

            /*
             * Set designers
             */
                try {
                    saveDesigners(connection, prodotto.getAutore(), id_prodotto);
                }catch(SQLException e){
                    filesManager.error("Saving designers");
                    e.printStackTrace();
                }
            /*
                Inserisco categorie
             */
                try {
                    saveCategories(connection, prodotto.getCategorie(), id_prodotto);
                } catch (SQLException e) {
                    filesManager.error("Saving categories");
                    e.printStackTrace();
                }
            /*
                Inserisco casa editrice
             */
                try{
                    savePublisher(connection,prodotto.getPublisher(),id_prodotto);
                }catch (SQLException e){
                    filesManager.error("Saving publishers");
                }

                //Ogni tot prodotti committo e inizio una nuova transazione.

                if(cont > Integer.parseInt(properties.getProperty("products_in_transaction"))){
                    stmt = connection.prepareStatement("COMMIT;");
                    stmt.execute();
                    cont = 0;
                    stmt = connection.prepareStatement("BEGIN;");
                    stmt.execute();
                }
            }
            stmt = connection.prepareStatement("COMMIT;");
            stmt.execute();

        } catch (IOException e) {
            e.printStackTrace();
            filesManager.error("During file reading");
        } catch (SQLException e) {
            filesManager.error("During insert main function");
            try {
                e.printStackTrace();
                stmt = connection.prepareStatement("ROLLBACK;");
                stmt.execute();
            } catch (SQLException e1) {
                filesManager.error("During rollback");
            }
        }


        try {
            if(stmt != null)
                stmt.close();
            connection.close();
        } catch (SQLException e) {
            filesManager.error("Closing connection or statement");
        }
    }

    /**
     * Riceve un elenco di categorie e lo aggiunge al database.
     * Essendo una relazione N a M vi è una tabella che mette in relazione
     * categorie e prodotti.
     *
     * @param connection connessione al database
     * @param categories Una lista di categorie
     * @param id_prodotto L'ID del prodotto
     * @throws SQLException
     */
    private void saveCategories(Connection connection,List<String> categories, Integer id_prodotto)throws SQLException{
        PreparedStatement statement = null;
        ResultSet rs;
        Integer category_id_to_insert = 0;
        try {
            for (String cat : categories) {
                statement = connection.prepareStatement("SELECT Category_ID FROM Categories WHERE Name = ?;");
                statement.setString(1,cat);
                //Ottengo l'id. Se non lo trovo devo inserire la categoria
                rs = statement.executeQuery();
                if (!rs.next()) {
                    //Categoria non presente. Da aggiungere.
                    statement = connection.prepareStatement("SELECT Name FROM Categories WHERE Category_ID = '-1' ");
                    //Ottengo l'ID da inserire
                    rs = statement.executeQuery();

                    while (rs.next()) {
                        try {
                            category_id_to_insert = Integer.parseInt(rs.getString("Name"));
                        }catch (NumberFormatException e){
                            filesManager.error("Eccezione durante la lettura del prossimo ID categorie da utilizzare. Forse non vi " +
                                    "è un record con chiave primaria -1 e un intero come nome");
                            return;
                        }
                    }

                    //La inserisco
                    statement = connection.prepareStatement("INSERT INTO Categories(Category_ID,Name) VALUES(?,?)");
                    statement.setInt(1,category_id_to_insert);
                    statement.setString(2,cat);
                    statement.executeUpdate();

                    //incremento l'id per la prossima insert
                    category_id_to_insert++;
                    System.out.println(category_id_to_insert);
                    statement = connection.prepareStatement("UPDATE Categories SET Name = " + category_id_to_insert + " WHERE Category_ID = '-1'");
                    statement.executeUpdate();
                    category_id_to_insert--;

                } else {
                    //Categoria presente. Prendo ID
                    category_id_to_insert = rs.getInt("Category_ID");
                }

                //aggiungo la relazione nella categoria prodotto
                statement = connection.prepareStatement(
                "INSERT INTO ProductsCategories(Product_ID,Category_ID) \n" +
                        "SELECT " + id_prodotto + ", '" + category_id_to_insert + "' \n" +
                        "WHERE NOT EXISTS(SELECT 1 FROM ProductsCategories WHERE Product_ID = '" + id_prodotto + "' AND Category_ID = '" + category_id_to_insert + "');");
                statement.executeUpdate();

            }
    }catch (SQLException e){
            rollBack(connection);
            filesManager.error("Saving categories");
            e.printStackTrace();
        }finally {
            if(statement != null)
                statement.close();
        }

    }

    /**
     *
     *
     * @param connection Connessione al database
     * @param publisher Casa editrice
     * @param id_prodotto Id del prodotto che fa parte delle categorie
     * @throws SQLException
     */
    private void savePublisher(Connection connection,String publisher, Integer id_prodotto)throws SQLException{
        PreparedStatement statement = null;
        ResultSet rs;
        Integer publisher_id_to_insert = 0;

        try {
                statement = connection.prepareStatement("SELECT Publisher_ID FROM Publishers WHERE Name = ?;");
                statement.setString(1,publisher);
                //Ottengo l'id. Se non lo trovo devo inserire la casa editrice
                rs = statement.executeQuery();
                if (!rs.next()) {
                    //Casa editrice non presente. Da aggiungere.
                    statement = connection.prepareStatement("SELECT Name FROM Publishers WHERE Publisher_ID = '-1' ");

                    //Ottengo l'ID da inserire
                    rs = statement.executeQuery();
                    while (rs.next()) {
                        try {
                            publisher_id_to_insert = Integer.parseInt(rs.getString("Name"));
                        }catch(NumberFormatException e){
                            filesManager.error("Eccezione durante la lettura del prossimo ID case editrici da utilizzare. Forse non vi " +
                                    "è un record con chiave primaria -1 e un intero come nome");
                            return;
                        }
                    }
                    //La inserisco
                    statement = connection.prepareStatement("INSERT INTO Publishers(Publisher_ID,Name) VALUES(?,?)");
                    statement.setInt(1,publisher_id_to_insert);
                    statement.setString(2,publisher);
                    statement.executeUpdate();

                    //incremento l'id per la prossima insert
                    publisher_id_to_insert++;
                    statement = connection.prepareStatement("UPDATE Publishers SET Name = " + publisher_id_to_insert + " WHERE Publisher_ID = '-1'  AND EXISTS (SELECT * FROM Publishers WHERE Publisher_ID = " + (publisher_id_to_insert - 1) + ")");
                    statement.executeUpdate();
                    publisher_id_to_insert--;

                } else {
                    //Categoria presente. Prendo ID
                    publisher_id_to_insert = rs.getInt("Publisher_ID");
                }

                //aggiungo la relazione nella categoria prodotto
                statement = connection.prepareStatement(
                        "INSERT INTO ProductsPublishers(Product_ID,Publisher_ID) \n" +
                                "SELECT " + id_prodotto + ", '" + publisher_id_to_insert + "' \n" +
                                "WHERE NOT EXISTS(SELECT 1 FROM ProductsPublishers WHERE Product_ID = '" + id_prodotto + "' AND Publisher_ID = '" + publisher_id_to_insert + "');");
                statement.executeUpdate();
        }catch (SQLException e){
            rollBack(connection);
            filesManager.error("Saving publishers");
            e.printStackTrace();
        }finally {
            if(statement != null)
                statement.close();
        }

    }


    private void saveDesigners(Connection connection,List<String> designers, Integer id_prodotto)throws SQLException{
        PreparedStatement statement = null;
        ResultSet rs;
        Integer designer_id_to_insert = 0;

        try {
            for(String designer : designers) {
                statement = connection.prepareStatement("SELECT Designer_ID FROM Designers WHERE Name = ?;");
                statement.setString(1, designer);
                //Ottengo l'id. Se non lo trovo devo inserire la casa editrice
                rs = statement.executeQuery();
                if (!rs.next()) {
                    //Casa editrice non presente. Da aggiungere.
                    statement = connection.prepareStatement("SELECT Name FROM Designers WHERE Designer_ID = '-1' ");
                    //Ottengo l'ID da inserire
                    rs = statement.executeQuery();
                    while (rs.next()) {
                        try {
                            designer_id_to_insert = Integer.parseInt(rs.getString("Name"));
                        }catch (NumberFormatException e){
                            filesManager.error("Eccezione durante la lettura del prossimo ID designer da utilizzare. Forse non vi " +
                                    "è un record con chiave primaria -1 e un intero come nome");
                            return;
                        }
                    }
                    //La inserisco
                    statement = connection.prepareStatement("INSERT INTO Designers(Designer_ID,Name) VALUES(?,?)");
                    statement.setInt(1, designer_id_to_insert);
                    statement.setString(2, designer);
                    statement.executeUpdate();

                    //incremento l'id per la prossima insert
                    designer_id_to_insert++;
                    statement = connection.prepareStatement("UPDATE Designers SET Name = " + designer_id_to_insert + " WHERE Designer_ID = '-1'  AND EXISTS (SELECT * FROM Designers WHERE Designer_ID = " + (designer_id_to_insert - 1) + ")");
                    statement.executeUpdate();
                    designer_id_to_insert--;

                } else {
                    //Categoria presente. Prendo ID
                    designer_id_to_insert = rs.getInt("Designer_ID");
                }

                //aggiungo la relazione nella categoria prodotto
                statement = connection.prepareStatement(
                        "INSERT INTO ProductsDesigners(Product_ID,Designer_ID) \n" +
                                "SELECT " + id_prodotto + ", '" + designer_id_to_insert + "' \n" +
                                "WHERE NOT EXISTS(SELECT 1 FROM ProductsDesigners WHERE Product_ID = '" + id_prodotto + "' AND Designer_ID = '" + designer_id_to_insert + "');");
                statement.executeUpdate();
            }
        }catch (SQLException e){
            rollBack(connection);
            filesManager.error("Saving publishers");
            e.printStackTrace();
        }finally {
            if(statement != null)
                statement.close();
        }

    }

    private void rollBack(Connection connection){
        Statement stmt;
        try {
            stmt = connection.createStatement();
            stmt.execute("ROLLBACK;");
        }catch(SQLException e){
            filesManager.error("During rollback");
        }
    }

    public void saveAlternativesImages(List<String> images, Connection connection, int id_prodotto)throws SQLException{
        PreparedStatement stmt;
        ResultSet rs;
        for (String alternativeImage : images) {
                        /*
                            Verifico se esiste già il record, e poi se non esiste lo inserisco. Sarebbe meglio farlo
                            con una sola query SQL, ma ottengo un errore "The prepared statment has been finalized".
                            Su stackoverflow viene segnalato un bug di SQLite che genera questo errore.
                            Forse è quello, forse sbaglio qualcosa. Nel dubbio così funziona e non dovrebbe essere troppo lento.
                         */
                stmt = connection.prepareStatement("SELECT * FROM AlternativeImages WHERE Product_ID = ? AND Image_Url = ? ;");
                stmt.setInt(1, id_prodotto);
                stmt.setString(2, "https:" + alternativeImage);
                rs = stmt.executeQuery();
                if(!rs.next()){
                    stmt = connection.prepareStatement("INSERT INTO AlternativeImages (Product_ID,Image_Url) VALUES( ?,? )");
                    stmt.setInt(1, id_prodotto);
                    stmt.setString(2, "https:" + alternativeImage);
                    stmt.executeUpdate();
                }


        }
    }
    /**
     * Restituisce una lista di emails a cui inviare la newsletter
     * @return List of subscribers
     */
    List<String> GetSubscribers(){
        List<String> ret = new ArrayList<String>();
        Statement statement;
        ResultSet resultSet;
        Connection connection = getDBConnection();

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM subscribers;");
            while (resultSet.next()) {
                ret.add(resultSet.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }

    /**
     * Restituisce una Lista  degli ultimi nproducts prodotti aggiunti al DataBase.
     * @param nproducts numero di prodotti da restituire
     * @return
     */
    List<Prodotto> GetNewAddedProducts(int nproducts){
        List<Prodotto> ret = new ArrayList<Prodotto>();
        Statement statement;
        Connection connection = getDBConnection();
        ResultSet resultSet;
        Prodotto temp;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT Product_ID,Name,Image_Url,Price,Description FROM Products ORDER BY AddDate DESC LIMIT " + nproducts + ";");
            while (resultSet.next()) {
                temp = new Prodotto();
                temp.setProductID(resultSet.getInt("Product_ID"));
                temp.setNome(resultSet.getString("Name"));
                temp.setPrezzo(resultSet.getString("Price"));
                temp.setImmagine(resultSet.getString("Image_Url"));
                temp.setDescrizione(resultSet.getString("Description"));
                ret.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            filesManager.error("Getting last added products");
        }
        System.out.println(ret);
        return ret;
    }
    private  Connection getDBConnection() {

        Connection dbConnection = null;

        try {

            Class.forName(properties.getProperty("sqlite_driver"));

        } catch (ClassNotFoundException e) {

            System.out.println("Errore nei driver");

        }

        try {

            dbConnection = DriverManager.getConnection(properties.getProperty("db_folder"));
            return dbConnection;
        } catch (SQLException e) {

            System.out.println("Errore nella get connection");

        }

        return null;

    }

}
