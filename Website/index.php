<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Gamair - I migliori giochi, al miglior prezzo</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>-->
    <script src="http://listjs.com/assets/javascripts/list.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/list.pagination.js/0.1.1/list.pagination.min.js"></script>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/gamair-custom.css" rel="stylesheet">
	</head>

  <body>
    <a name="top_anchor"></a>
    <nav class="navbar navbar-default" style="margin-bottom: 0;" >
      <div class="container-fluid">
          <a class="navbar-brand" href="index.php" style="padding-top: 18px; padding-right:10px">
              <img src="images/logogamair.gif" width="30" height="30" alt="">
          </a>

    			<div class="navbar-header">
    			  	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
    			    	<span class="sr-only">Toggle navigation</span>
    			    	<span class="icon-bar"></span>
    			    	<span class="icon-bar"></span>
    			    	<span class="icon-bar"></span>
    			  	</button>
    			  	<a class="navbar-brand" href="index.php">GAMAIR</a>
    			</div>

    			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    			  	<ul class="nav navbar-nav">
    			    	<li class="active"><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
    			    	<li><a href="contattaci.php">Contattaci</a></li>
    			  	</ul>

    			  	<ul class="nav navbar-nav navbar-right" >
    					<form action="index.php" name="search" method="GET" class="navbar-form navbar-left" style="margin:10px 0px 0px 0px;">
    						<div class="form-group">
    						  	<div id="custom-search-input">
    				                <div class="input-group col-md-12">
    				                    <input id="inputSearch" name="search" type="text" class="form-control input-lg" placeholder="Cerca..." />
    				                    <span class="input-group-btn">
    				                        <button class="btn btn-info btn-lg" type="submit" style="background-color: orange">
    				                            <span class="glyphicon glyphicon-search"></span>
    				                        </button>
    				                    </span>
    				                </div>
    				        </div>
    						</div>
    				  </form>
    		     	</ul>
	         </div><!-- navbar -->
	  	</div><!-- container fluid-->
	  </nav>

	<div class="jumbotron" style="margin:0px; padding:50px; background-image: url('images/background.jpg')">
	    <h1>Scopri ora le novit&agrave;</h1>
	    <p>I migliori giochi da tavolo, di carte e di societ&agrave; da tutto il mondo, al miglior prezzo</p>
	    <p><a class="btn btn-warning btn-lg" href="landing.php" role="button">Scopri di pi&ugrave;</a></p>
	</div>

  <?php
      require 'include/functions.php';

      $db = new GamairDB();
      if(!$db){
        //echo $db->lastErrorMsg();
      } else {
        //echo "Database aperto <br>";
      }

      $category = "0";                    //Default category is 0, that means "all the categories"
      $orderedtype = "Decrescente";       //Default order label is Decreasing
      $orderedby = "Popolarita";          //Default order by is popularity
      $searchtext = "";                   //Default search text is ""
      $orderquery = "DESC";               //Default order type is DESC, that means decreasing

      if(isset($_GET['search'])){
          if ($_GET['search']!=""){           //If search variable (in the GET call) is not "", set $searchtext
          //echo $_GET['search'];
          $searchtext = $_GET['search'];
        }
      }

      if(isset($_GET['category'])){
        if($_GET['category']!=""){          //If category variable (in the GET call) is not "", set $category
          $category = $_GET['category'];
        }
      }

      if(isset($_GET['type'])){
        if($_GET['type']!=""){              //If type variable (in the GET call) is not "", set £orderedtype and $orderquery
          if ($_GET['type']=="Decrescente"){
            $orderedtype="Decrescente";
            $orderquery="DESC";
          }else {
            $orderedtype="Crescente";
            $orderquery="ASC";
          }
        }
      }

      if(isset($_GET['by'])){
        if($_GET['by']!=""){               //If by variable (in the GET call) is not "", set $orderedby
          $orderedby=$_GET['by'];
        }
      }


      switch ($orderedby) {               //let's check which one is selected and let's make query string
        case 'Popolarita':
          if(!$category){
            $QuerySQL = "SELECT *
                         FROM Products
                         WHERE Name COLLATE UTF8_GENERAL_CI LIKE '%$searchtext%'
                         ORDER BY Hit $orderquery;";
          }else{
            $QuerySQL = "SELECT Products.Product_ID,Product_Url, Name, Image_Url, Description, Price, MinAge, MinPlayers, MaxPlayers, Production_Year, Hit, AddDate, Category_ID
                        FROM Products
                        INNER JOIN ProductsCategories
                        ON Products.Product_ID = ProductsCategories.Product_ID
                        WHERE Name COLLATE UTF8_GENERAL_CI LIKE '%$searchtext%' AND Category_ID=$category
                        ORDER BY Hit $orderquery;";
          }
          break;

        case 'Prezzo':
          if(!$category){
            $QuerySQL = "SELECT *
                         FROM Products
                         WHERE Name COLLATE UTF8_GENERAL_CI LIKE '%$searchtext%'
                         ORDER BY Price $orderquery;";
          }else{
            $QuerySQL = "SELECT Products.Product_ID,Product_Url, Name, Image_Url, Description, Price, MinAge, MinPlayers, MaxPlayers, Production_Year, Hit, AddDate, Category_ID
                        FROM Products
                        INNER JOIN ProductsCategories
                        ON Products.Product_ID = ProductsCategories.Product_ID
                        WHERE Name COLLATE UTF8_GENERAL_CI LIKE '%$searchtext%' AND Category_ID=$category
                        ORDER BY Price $orderquery;";
          }
          break;

        case 'Eta':
          if(!$category){
            $QuerySQL = "SELECT *
                         FROM Products
                         WHERE Name COLLATE UTF8_GENERAL_CI LIKE '%$searchtext%'
                         ORDER BY MinAge $orderquery;";
          }else{
            $QuerySQL = "SELECT Products.Product_ID,Product_Url, Name, Image_Url, Description, Price, MinAge, MinPlayers, MaxPlayers, Production_Year, Hit, AddDate, Category_ID
                        FROM Products
                        INNER JOIN ProductsCategories
                        ON Products.Product_ID = ProductsCategories.Product_ID
                        WHERE Name COLLATE UTF8_GENERAL_CI LIKE '%$searchtext%' AND Category_ID=$category
                        ORDER BY MinAge $orderquery;";
          }
          break;

        case 'NGiocatori':
          if(!$category){
            $QuerySQL = "SELECT *
                         FROM Products
                         WHERE Name COLLATE UTF8_GENERAL_CI LIKE '%$searchtext%'
                         ORDER BY MinPlayers $orderquery;";
          }else{
            $QuerySQL = "SELECT Products.Product_ID,Product_Url, Name, Image_Url, Description, Price, MinAge, MinPlayers, MaxPlayers, Production_Year, Hit, AddDate, Category_ID
                        FROM Products
                        INNER JOIN ProductsCategories
                        ON Products.Product_ID = ProductsCategories.Product_ID
                        WHERE Name COLLATE UTF8_GENERAL_CI LIKE '%$searchtext%' AND Category_ID=$category
                        ORDER BY MinPlayers $orderquery;";
          }
          break;

        case 'Anno':
         if(!$category){
           $QuerySQL = "SELECT *
                        FROM Products
                        WHERE Name COLLATE UTF8_GENERAL_CI LIKE '%$searchtext%'
                        ORDER BY Production_Year $orderquery;";
          }else{
            $QuerySQL = "SELECT Products.Product_ID,Product_Url, Name, Image_Url, Description, Price, MinAge, MinPlayers, MaxPlayers, Production_Year, Hit, AddDate, Category_ID
                        FROM Products
                        INNER JOIN ProductsCategories
                        ON Products.Product_ID = ProductsCategories.Product_ID
                        WHERE Name COLLATE UTF8_GENERAL_CI LIKE '%$searchtext%' AND Category_ID=$category
                        ORDER BY Production_Year $orderquery;";
          }
          break;

        default:
          $orderedby = "Popolarita";
          if(!$category){
            $QuerySQL = "SELECT *
                         FROM Products
                         WHERE Name COLLATE UTF8_GENERAL_CI LIKE '%$searchtext%'
                         ORDER BY Hit $orderquery;";
          }else{
            $QuerySQL = "SELECT Products.Product_ID,Product_Url, Name, Image_Url, Description, Price, MinAge, MinPlayers, MaxPlayers, Production_Year, Hit, AddDate, Category_ID
                        FROM Products
                        INNER JOIN ProductsCategories
                        ON Products.Product_ID = ProductsCategories.Product_ID
                        WHERE Name COLLATE UTF8_GENERAL_CI LIKE '%$searchtext%' AND Category_ID=$category
                        ORDER BY Hit $orderquery;";
          }
          break;
      }

      echo '<div class="panel panel-warning">';                                                     //opening panel warning
        $home = new ProductManager();                                                              //create new product manager
        echo $home->showHeader($category,  $orderedtype , $orderedby, $searchtext); //use product manager method showHeader to show panel header
        echo '<div class="panel-body">';                                                           //opening panel body

      //make query string
      $sql =<<<EOD
      $QuerySQL;
EOD;

	   	$ret = $db->query($sql);                                                             //executing query
      $row = $ret->fetchArray(SQLITE3_ASSOC);                                              //fetching results
      if($row){                                                                            //if there is at least one product
          echo '<div id="product-list">
                  <ul class="list flex-row" style="padding:0px;">';                        //print flex row (same height thumbnails) and product list

          if($row['Product_ID']!=-1){                                                      //if product id is not -1, then show first thumbnail
            echo $home->showThumbnail($row['Product_ID'], $row['Description'], $row['Image_Url'], $row['Name'], $row['Price'], $row['Product_Url']);
          }
          $count=0;
          while(($row = $ret->fetchArray(SQLITE3_ASSOC)) && $count<1500){                 //print thumbnails until count arrives at 1500 and there are products in $ret
              //if product id is not -1, then show thumbnail
              if($row['Product_ID']!=-1){
                //show thumbnail using ProductManager ($home) method
                echo $home->showThumbnail($row['Product_ID'], $row['Description'], $row['Image_Url'], $row['Name'], $row['Price'], $row['Product_Url']);
              }
              $count++;                                                                   //increase count
    	   	}

          echo      '</ul>
                    <div class="PaginationWrapper">
                        <ul class="pagination"></ul>
                    </div>
                </div>';
      }else{
        //
        new Alert("Nessun prodotto trovato con i criteri di ricerca selezionati","danger");
      }

   	  $db->close();
?>
        <!-- Button to return to top -->
        <div style="text-align:right; margin-right:10px;">
          <a href="#top_anchor" class="btn btn-warning btn-lg">
            <span class="glyphicon glyphicon-chevron-up"></span>
          </a>
        </div>
      </div>
    </div>

	  <footer style="padding-bottom:20px" align="center">&copy; Copyright 2016<br>ITIS Cuneo Mario Delpozzo</footer>

    <!-- Script for pagination -->
    <script>
      var monkeyList = new List('product-list', {
        valueNames: ['product'],
        page: 12,
        plugins: [ ListPagination({}) ]
    });
    </script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
