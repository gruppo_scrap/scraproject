<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Gamair - I migliori giochi, al miglior prezzo</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link href="css/gamair-custom.css" rel="stylesheet">
    </head>

    <body>
      	  <!-- Top menu -->
          <nav class="navbar navbar-default" style="margin-bottom: 0;" >
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php" style="padding-top: 18px; padding-right:10px">
                    <img src="images/logogamair.gif" width="30" height="30" alt="">
                </a>
    			      <div class="navbar-header">
    			  	      <a class="navbar-brand" href="index.php">GAMAIR</a>
  	            </div>
            </div>
      	  </nav>

            <div style="padding:30px;" >
                <div class="container">
                    <div class="row">
                        <div class="col-sm-7" style="padding: 20px;">
                            <h1>I migliori Giochi <br>Al miglior prezzo</h1>
                            <p>
	                            	<b>GAMAIR</b> è una piattaforma che raccoglie i giochi in scatola da diversi store online e ti consente di cercare il prodotto che ti interessa e di acquistarlo al miglior prezzo
                            </p>
                            <a href="index.php" class="btn btn-warning" role="button">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                &nbsp;Vai alla Home
                            </a>
                            <a href="contattaci.php" class="btn btn-warning" role="button">
                                  Contattaci
                            </a>
                        </div>

                        <div class="col-sm-5 form-box"  style="padding: 20px;">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Iscriviti alla newsletter</h3>
                            		<p>Per rimanere aggiornato su tutte le novità</p>
                        		</div>
                        		<div class="form-top-right">
                        			<span aria-hidden="true" class="typcn typcn-pencil"></span>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                         <form role="form" action="landing.php" method="post">
                                  <div class="form-group">
                                    <input type="text" name="subscriber_name" placeholder="Nome e Cognome" class="form-control" id="form-name">
                                  </div>
    			                        <div class="form-group">
    			                          <input type="email" name="subscriber_email" placeholder="Email" class="form-control" id="form-email">
    			                       </div>
    			                       <button type="submit" class="btn btn-warning">Conferma</button>
    			                       <div class="form-links" style="margin-top: 7px;">
    			                        	Confermando accetti la <a href="privacy.html">Privacy Policy</a>
    			                       </div>
			                         </form>
		                        </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php
           if($_SERVER['REQUEST_METHOD']=='POST'){                                                      //if there has been a POST request
               require 'include/functions.php';                                                         //require functions.php --> group of class and methods
               if(trim($_POST['subscriber_email']) && trim($_POST['subscriber_name'])){                 //if email and name are setted
                 $news = new Newsletter();                                                              //create new newsletter
                 $news->subscribe($_POST['subscriber_email'], $_POST['subscriber_name']);               //subscribe
               }else{
                 new Alert("Attenzione, controlla di aver compilato correttamente i campi", "danger");  //show an alert if the user doesn't complete all the fields
               }
           }
        ?>
        <hr>

        <!-- Features -->
        <div class="container">
            <div class="row">
            	  <div class="col-sm-4 feature">
              		<img src="images/tap.png" width="100px">
                  <h3>Semplice da usare</h3>
                  <p>Grazie ad un interfaccia intuitiva e semplice effettua ricerche tra migliaia di prodotti, ordinali per prezzo, numero di giocatori, popolarit&agrave;, et&agrave; oppure anno di uscita</p>
                </div>

                <div class="col-sm-4 feature">
              		<img src="images/mobile.png" width="100px">
                  <h3>Mobile-Friendly</h3>
                  <p>Utilizza GAMAIR da pc, tablet e smartphone senza installare applicazioni aggiuntive, semplicemente visitando la pagina da un normale browser</p>
                </div>

                <div class="col-sm-4 feature">
              		<img src="images/sync.png" width="100px">
                  <h3>Sempre Aggiornata</h3>
                  <p>Grazie ai nostri software, GAMAIR viene aggiornato continuamente con nuovi prodotti e prezzi attendibili, per massimizzare il risparmio nei tuoi acquisti</p>
                </div>
            </div>
            <div class="row">
            	 <div class="col-sm-4 feature">
              		<img src="images/chatting.png" width="100px">
                  <h3>Grande community</h3>
                  <p>Consulta le recensioni del prodotto che stai cercando prima di acquistarlo</p>
                </div>

                <div class="col-sm-4 feature">
              		<img src="images/letter.png" width="100px">
                  <h3>Newsletter</h3>
                  <p>Iscriviti alla newsletter e ricevi aggiornamenti via email riguardanti prodotti in uscita o sconti lampo</p>
                </div>

                <div class="col-sm-4 feature">
              		<img src="images/customer-service.png" width="100px">
                  <h3>Customer Service</h3>
                  <p>Hai problemi con la piattaforma o hai consigli? <br><a href="contattaci.php" >Contattaci</a> e ti ascolteremo</p>
                </div>
            </div>
        </div>

        <hr>
        <!-- Footer -->
        <footer align="center" style="padding-bottom:25px">&copy; Copyright 2016<br>ITIS Cuneo Mario Delpozzo</footer>
    </body>
</html>
