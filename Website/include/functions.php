<?php
/*
    functions.php
    version    1.0
    Functions.php contains boxed features to simplify code in the other pages
*/

    /**
    * GamairDB is the extension of SQLite3 to allow the use of Gamair Database
    * @version    1.0
    */
    class GamairDB extends SQLite3{
      function __construct()
      {
        //open the SQLite database database.db in database folder
        $this->open('database/database.db');
      }
    }

    /**
    * Show a bootstrap alert depending on the type passed (danger, success, ecc...)
    * @version    1.0
    */
    class Alert{
      /**
       * __construct
       *
       * Print into the HTML document a bootstrap alert depending on the type passed (danger, success, ecc...)
       *
       * @param string $message Message that need to be printed
       * @param string $type Bootstrap alert type (danger, success, ecc...)
       */
      function __construct($message, $type){
        echo '<div class="alert alert-'.$type.'" role="alert" style="margin-top: 10px;">
                '.$message.'
              </div>';
      }
    }

    /**
    * Newsletter class can subscribe or unsubscribe people and get name having the email
    * @version    1.0
    */
    class Newsletter{
      /**
       * subscribe
       *
       * Add an email and name row into the database depending on whether it has done or not
       *
       * @param string $email Subscriber email
       * @param string $name Subscriber name
       */
        public function subscribe($email, $nome){
          $db = new GamairDB();           //create new database connection

          $sql =<<<EOD
            INSERT INTO Subscribers
            VALUES ('$email', '$nome');
EOD;
          //WARNING: don't put anything after EOD in the same line
          //EOD --> alternative manner to write multiple rows

          //SQL query check if success or not
          if($db->exec($sql)){
            new Alert('Grazie '.$nome.', sei ora iscritto alla newsletter', "success");
          }else{
            new Alert('Attenzione '.$nome.', c&rsquo;&egrave; stato un problema nell&rsquo;iscrizione alla newsletter', "danger");
          }

          $db->close();
        }

        /**
         * unsubscribe
         *
         * Remove an email and name row from the database and print an alert depending on whether it has done or not
         *
         * @param string $email Unsubscriber email
         */
        public function unsubscribe($email){
          $db = new GamairDB();     //open database connection
          $sql =<<<EOD
            DELETE FROM Subscribers
            WHERE email='$email';
EOD;
            //WARNING: don't put anything after EOD in the same line
            //EOD --> alternative manner to write multiple rows

            $nome = $this->getName($email);

            //SQL query check if success or not
            if($db->exec($sql)){
              $alertDataPost = new Alert('L&rsquo;operazione è andata a buon fine! Speriamo di rivederti presto '. $nome .'', "success");
            }else{
              $newsAlert = new Alert('Attenzione '.$nome.', si &egrave; verificato un problema nella richiesta', "danger");
            }
          $db->close();
        }

        /**
         * getName
         *
         * Returns the string corresponding to the requested name
         *
         * @param string $email Subscriber email
         * @return string $name Subscriber name
         */
        public function getName($email){
          $db = new GamairDB();     //create new database connection
          $sql =<<<EOD
            SELECT * FROM Subscribers
            WHERE email='$email';
EOD;
          //WARNING: don't put anything after EOD in the same line
          //EOD --> alternative manner to write multiple rows

          $ret = $db->query($sql);                      //execute query
          $names = $ret->fetchArray(SQLITE3_ASSOC);     //fetch query returned array
          $name = $names['name'];
          if($name==""){                                //check name string
            $name=$email;
          }
          $db->close();                                 //close database
          return $name;
        }
      }


      /**
      * Send an email to Gamair STAFF
      * @version    1.0
      */
      class Email{
        /**
         * __construct
         *
         * Use PHPMailer plugin and send an email to GAMAIR STAFF
         *
         * @param string $mail_mittente Sender email
         * @param string $nome_mittente Sender name
         * @param string $mail_corpo Message body
         */
        function __construct($mail_mittente, $nome_mittente, $mail_corpo){
           require 'include/PHPMailer/PHPMailerAutoload.php';
           $mail = new PHPMailer;

           //$mail->SMTPDebug = 3;                                    // Enable verbose debug output (show lines with sending details)
           $mail->isSMTP();                                           // Set mailer to use SMTP
           $mail->Host = 'smtp.gmail.com';                            // Specify main and backup SMTP servers
           $mail->SMTPAuth = true;                                    // Enable SMTP authentication
           $mail->Username = 'pomoyolo@gmail.com';                    // SMTP username
           $mail->Password = 'gamair000';                             // SMTP password
           $mail->SMTPSecure = 'tls';                                 // Enable TLS encryption, `ssl` also accepted
           $mail->Port = 587;                                         // TCP port to connect to
           $mail->setFrom('pomoyolo@gmail.com', $nome_mittente );     // Set from
           $mail->addAddress('pomoyolo@gmail.com');                   // Add a recipient
           $mail->addReplyTo($mail_mittente, $nome_mittente);         // Add reply to
           $mail->isHTML(true);                                       // Set email format to HTML
           $mail->Subject = "GAMAIR - New Message";                   // Set email subject
           $mail->Body    = $mail_corpo;                              // Set email body
           $mail->AltBody = $mail_corpo;                              // Set body in plain text for non-HTML mail clients

           //send email
           if(!$mail->send()){
             $inputAlert = new Alert('<strong>Attenzione!</strong> Controlla di aver completato correttamente i campi', "danger");
           }else{
             $messageAlert = new Alert('Il messaggio è stato inviato correttamente.
                                        Ti ricontatteremo all&rsquo;indirizzo '.$mail_mittente.' se necessario.', "success");
           }
        }

      }

      /**
      * Create a group of stars for rating
      * @version    1.0
      */
      class StarsGroup{
        /**
         * __construct
         *
         * Print into the HTML document a group of stars (filled or not depending on the rating)
         *
         * @param Integer $number Number of filled stars
         */
        function __construct($number){
          $rating="";
          //print n filled stars depending on the $number
          for ($i=0; $i<$number; $i++){
            $rating.='<span class="glyphicon glyphicon-star"></span>';
          }

          //print n not filled remaining stars
          for(; $i<5; $i++){
            $rating.='<span class="	glyphicon glyphicon-star-empty"></span>';
          }

          echo $rating;
        }
      }


      /**
      * Convert a string into a price
      * @version    1.0
      */
      class Price{
        /**
         * convert
         *
         * Convert a string into a price, adding € and , --> "6589". Will become €65,89
         *
         * @param string $text Product price stored in database like
         * @return string $ret String formatted with € and ,
         */
        function convert($text){
          switch (strlen($text)) {
            //if we don't have numbers
            case 0:
              $ret = "Prezzo non disponibile";
              break;

            //if the number has 1 digit
            case 1:
              $ret = "&euro; 0,0".$text;
              break;

            //if the number has 2 digit
            case 2:
              $ret = "&euro;0,".$text;
              break;

            //if the number has n digit and not 1,2 or 3
            default:
              $ret = "&euro;".substr_replace($text, ",", strlen($text)-2, 0);
              break;
          }
          return $ret;
        }
      }

      /**
      * Add product review, print reviews, get medium rating, random string for review primary key and get reviews number
      * @version    1.0
      */
      class Review{
          /**
           * addReview
           *
           * Add a review into the database
           *
           * @param integer $rating Product rating
           * @param string $comments Review comments
           * @param string $name Reviewer name
           * @param string $idprod Product primary key
           * @param GamairDB $db Database
           */
          public function addReview($rating, $comments, $name, $idprod, $db){
            //if parameters are setted
            if(trim($rating)  && trim($comments) && trim($name) && trim($idprod)){
              $idrec = $this->generatePK($name);    // generate a primary key
              $date = date('c');                    // get current date in ISO 8601 date (added in PHP 5)	-> 2004-02-12T15:19:21+00:00
              $sql =<<<EOD
                INSERT INTO Reviews
                VALUES ('$idrec', '$idprod', '$comments', '$rating', '$name', '$date');
EOD;
              //check SQL instruction for database
              if($db->exec($sql)){
                new Alert("Complimenti! ".$name.", la tua recensione è stata inviata con successo", "success");
              }else{
                new Alert("Attenzione! ".$name.", la tua recensione non è stata inviata correttamente", "danger");
              }
            }else{
              new Alert("Attenzione! Controlla di aver completato tutti i campi", "danger");
            }
          }

          /**
           * printReviews
           *
           * This function print into the document the list of reviews, getting product id
           *
           * @param string $id Product primary key
           */
          public function printReviews($id){
            $database = new GamairDB();     //create a new database connection
            $sqlRev =<<<EOD
              SELECT * FROM Reviews
              WHERE IDProd='$id'
              ORDER BY Date DESC;
EOD;
      	   	$revArray = $database->query($sqlRev);   //execute SQL query

            //Check if there's at least one review
            $reviewSingle = $revArray->fetchArray(SQLITE3_ASSOC);
            if(!$reviewSingle){
              echo '<li class="list-group-item">';
              echo '<div><b>Nessuna recensione disponibile</b></div>';
              echo '</li>';
            }else{
              echo '<li class="list-group-item">';
              new StarsGroup($reviewSingle['Rating']);
              echo '<div><b>'.$reviewSingle['Name'].'</b></div>';
              echo '<div>'.$reviewSingle['Comments'].'</div>';
              echo '</li>';
            }

            //if there are more than one reviews, print them
      	   	while(($reviewSingle = $revArray->fetchArray(SQLITE3_ASSOC))){
              echo '<li class="list-group-item">';                  //create list-group-item
              new StarsGroup($reviewSingle['Rating']);              //create group of stars filled or not depending on the Rating
              echo '<div><b>'.$reviewSingle['Name'].'</b></div>';   //printing reviewer name
              echo '<div>'.$reviewSingle['Comments'].'</div>';      //printing reviewer comments
              echo '</li>';
            }
            $database->close();                                     //closing database
          }

          /**
           * generatePK
           *
           * returns the current Unix timestamp with microseconds
           *
           * @return string current Unix timestamp with microseconds
           */
          private function generatePK($name){
            $ret = microtime(false);    //unix timestamp
            if(trim($name)==""){
              $name = str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789"); //random string with char and numbers
            }
            $ret.=$name;                //concat name and ret
            return $ret;
          }

          /**
           * getReviewsNumber
           *
           * Return the number of reviews from a product id
           *
           * @param string $id Product Primary key
           * @param GamairDB $db Database opened
           * @return Integer $numRows Reviews Number
           */
          public function getReviewsNumber($id, $db){
            $sql =<<<EOD
              SELECT COUNT(Rating) as count
              FROM Reviews
              WHERE IDProd='$id';
EOD;
              $rows = $db->query($sql);   //execute query
              $row = $rows->fetchArray(); //fetch results
              $numRows = $row['count'];   //get reviews count
              return $numRows;
          }

          /**
           * getMediumRating
           *
           * Return average product rating
           *
           * @param string $id Product Primary key
           * @param GamairDB $db Database opened
           * @return Integer $avg Average rating
           */
          public function getMediumRating($id, $db){
            $sql =<<<EOD
              SELECT AVG(Rating) as average
              FROM Reviews
              WHERE IDProd='$id';
EOD;
              $rows = $db->query($sql);     //execute query
              $row = $rows->fetchArray();   //fetch results
              $avg = $row['average'];       //get rating average
              return $avg;                  //return rating average
          }
      }


      /**
      * ProductManager
      *
      * Used in the homepage and in productpage for printing thumbnails, checking strings, ecc...
      *
      * @version    1.0
      */
      class ProductManager{
        /**
         * convertOrderBy
         *
         * Convert a string that come from a php GET call into a printable text for dropdown buttons
         *
         * @param string $by raw data from GET call
         * @return string $ret formatted data for dropdown button
         */
        function convertOrderBy($by){
          switch ($by) {
            case 'Popolarita':
              $ret = "Popolarit&agrave;";
              break;

            case 'Prezzo':
              $ret = "Prezzo";
              break;

            case 'Eta':
              $ret = "Et&agrave;";
              break;

            case 'NGiocatori':
              $ret = "Numero Giocatori";
              break;

            case 'Anno':
              $ret = "Anno di uscita";
              break;

            default:
              $ret = "Popolarit&agrave;";
              break;
          }
          return $ret;
        }

        /**
         * incrementHits
         *
         * Increment hits when a productpage is opened
         *
         * @param Integer $id Product id
         */
        public function incrementHits($id, $db){
          //create sql string to increment product hits
          $sql =<<<EOD
            UPDATE Products
            SET Hit=Hit+1
            WHERE Product_ID=$id;
EOD;
            $db->exec($sql);      //execute SQL
        }

        /**
         * getCategories
         *
         * Return a HTML formatted string with the categories into a list
         *
         * @param string $selectedcategory Selected category ID
         * @param string $in_de_creasing Increasing or decreasing
         * @param string $orderedby Order by
         * @param string $searchtxt Searched text
         * @return string $ret formatted HTML list
         */
        function getCategories($selectedcategory, $in_de_creasing, $orderedby, $searchtxt){
          $db = new GamairDB();                             //create new database
          $ret="";                                          //set $ret at ""
                                                            //create sql string
          $sql =<<<EOD
            SELECT *
            FROM Categories;
EOD;
          $cats = $db->query($sql);                         //execute sql query
          while($cat = $cats->fetchArray(SQLITE3_ASSOC)){   //for every review, print details
            if($cat['Category_ID']!=-1){
              $catName = ucfirst($cat['Name']);             //Every first letter uppercase
              $ret.='<li><a href="./index.php?search='.$searchtxt.'&category='.$cat['Category_ID'].'&type='.$in_de_creasing.'&by='.$orderedby.'">'.$catName.'</a></li>';
            }
            //ret is now a list of <li> with all the categories
          }
          $db->close();                                     //close database connection
          return $ret;
        }



        /**
         * idToCat
         *
         * Converts an integer ID into a string with the corresponding categoty
         *
         * @param integer $id category id
         * @return string $ret corresponding category
         */
        function idToCat($id){
          if($id==0 || (!trim($id))){
            $ret = "Tutte le categorie";
          }else{
            $db = new GamairDB();                           //create new database connection
            $ret="";                                        //set $ret at ""
            $sql =<<<EOD
              SELECT Name
              FROM Categories
              WHERE Category_ID='$id';
EOD;
            $catString = $db->query($sql);                  //execute sql string
            $row = $catString->fetchArray(SQLITE3_ASSOC);   //fetch results
            $ret = ucfirst($row['Name']);                   //uppercase of name
            $db->close();                                   //close database
          }
          return $ret;
        }


        /**
         * idToPub
         *
         * Converts an integer ID into a string with the corresponding publisher
         *
         * @param integer $id category id
         * @return string $ret corresponding publisher
         */
        function idToPub($id){
          if(($id==-1) || (!trim($id))){
            $ret = "Produttore Sconosciuto";                //if publisher is not known
          }else{
            $db = new GamairDB();                           //create new database connection
            $ret="";                                        //set $ret at ""
            $sql =<<<EOD
              SELECT *
              FROM Publishers
              WHERE Publisher_ID='$id';
EOD;
            $catString = $db->query($sql);                  //execute sql
            $row = $catString->fetchArray(SQLITE3_ASSOC);   //fetching results
            $ret = ucfirst($row['Name']);                   //all uppercase
            $db->close();                                   //close database connection
          }
          return $ret;
        }

        /**
         * idToDes
         *
         * Converts an integer ID into a string with the corresponding Designer
         *
         * @param integer $id designer id
         * @return string $ret corresponding Designer
         */
        function idToDes($id){
          if($id==-1 || (!trim($id))){
            $ret = "Designer Sconosciuto";                    //if designer is not known
          }else{
            $db = new GamairDB();                             //create new database connection
            $ret="";                                          //set $ret at ""
            $sql =<<<EOD
              SELECT *
              FROM Designers
              WHERE Designer_ID='$id';
EOD;
            $catString = $db->query($sql);                    //execute query string
            $row = $catString->fetchArray(SQLITE3_ASSOC);     //fetching results
            $ret = ucfirst($row['Name']);                     //uppercase
            $db->close();
          }
          return $ret;
        }



        /**
         * showHeader
         *
         * Shows panel header (called from the home page)
         *
         * @param string $selectedcategory Selected category
         * @param string $in_de_creasing Order type: increasing or decreasing
         * @param string $orderedby Order by: Price, Popularity, Age, Number of players, etc...
         * @param string $searchtxt String with search text
         * @return string formatted data for panel header
         */
        function showHeader($selectedcategory, $in_de_creasing, $orderedby, $searchtxt){
          $labelSelectedCategory = $this->idToCat($selectedcategory);       //convert an integer id into text
          $labelSelectedOrderBy = $this->convertOrderBy($orderedby);        //convert an orderBy string into a printable orderby string
          $categoriesList = $this->getCategories($selectedcategory, $in_de_creasing, $orderedby, $searchtxt);       //get a string built with <li> containing categories
        return '<div class="panel-heading">
              <div class="row">
                <div class="dropdown" style="display:inline-block; float: left; margin-left: 10px">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:gray; margin-top:2px; margin-bottom:2px">
                      '.$labelSelectedCategory.'
                      <span class="caret"></span>
                    </button>

                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      <li><a href="./index.php?search='.$searchtxt.'&category=0&type='.$in_de_creasing.'&by='.$orderedby.'">Tutte le categorie</a></li>
                      <li role="separator" class="divider"></li>
                      '.$categoriesList.'
                    </ul>
                </div>

                <div class="dropdown" style="display:inline-block; float: right; margin-right: 10px">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:gray; margin-top:2px; margin-bottom:2px">
                      '.$in_de_creasing.'
                      <span class="caret"></span>
                    </button>

                     <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                         <li><a href="./index.php?search='.$searchtxt.'&category='.$selectedcategory.'&type=Crescente&by='.$orderedby.'">Crescente</a></li>
                         <li><a href="./index.php?search='.$searchtxt.'&category='.$selectedcategory.'&type=Decrescente&by='.$orderedby.'">Decrescente</a></li>
                     </ul>
                </div>

                <div class="dropdown" style="display:inline-block; float: right; margin-right: 10px">
                    <button id="dropOrderBy" class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:gray; margin-top:2px; margin-bottom:2px">
                      '.$labelSelectedOrderBy.'
                      <span class="caret"></span>
                    </button>

                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
                        <li><a href="./index.php?search='.$searchtxt.'&category='.$selectedcategory.'&type='.$in_de_creasing.'&by=Popolarita">Popolarit&agrave;</a></li>
                        <li><a href="./index.php?search='.$searchtxt.'&category='.$selectedcategory.'&type='.$in_de_creasing.'&by=Prezzo">Prezzo</a></li>
                        <li><a href="./index.php?search='.$searchtxt.'&category='.$selectedcategory.'&type='.$in_de_creasing.'&by=Eta">Et&agrave;</a></li>
                        <li><a href="./index.php?search='.$searchtxt.'&category='.$selectedcategory.'&type='.$in_de_creasing.'&by=NGiocatori">Numero giocatori</a></li>
                        <li><a href="./index.php?search='.$searchtxt.'&category='.$selectedcategory.'&type='.$in_de_creasing.'&by=Anno">Anno di uscita</a></li>
                    </ul>
                </div>
              </div>
            </div> <!-- end of the panel heading-->';
        }

        /**
         * checkDescr
         *
         * Check if the description passed is null, and cut it if it's longer than 270 characters
         *
         * @param string $text text to test
         * @return string $descr product description checked
         */
        function checkDescr($text){
          if($text==""){
            $descr="Descrizione non disponibile";
          }else{
            if(strlen($text)>270){
              $descr = substr($text, 0, 270 )."...";      //cut the description string at 270 characters and add ...
            }else{
              $descr = $text;
            }
          }
          return $descr;
        }

        /**
         * checkName
         *
         * Check if the name passed is null, and cut it if it's longer than 30 characters
         *
         * @param string $text text to test
         * @return string $name product name checked
         */
        function checkName($text){
          if($text==""){
            $descr="Nome non disponibile";
          }else{
            if(strlen($text)>30){
              $name = substr($text, 0, 30 )."...";      //cut the name string at 30 characters and add ...
            }else{
              $name = $text;
            }
          }
          return $name;
        }

        /**
         * checkURL
         *
         * Check if the URL passed is null, and fix it with an #
         *
         * @param string $text text to test
         * @return string $name product URL checked
         */
        function checkURL($text){
          if(strlen($text)){
            $url = $text;
          }else{
            $url = "#";                                 //if url is null set #
          }
          return $url;
        }

        /**
         * showThumbnail
         *
         * Return the html code for thumbnails
         *
         * @param string $id product id
         * @param string $descr product description
         * @param string $image product image url
         * @param string $name product name
         * @param string $price product price
         * @param string $url product url
         * @return string HTML code
         */
        function showThumbnail($id, $descr, $image, $name, $price, $url){
            $productImg = $image;                                 //product image url
            $productDescr = $this->checkDescr($descr);            //product description
            $productName = $this->checkName($name);               //product name
            $priceConverter = new Price();                        //product price CONVERTER
            $productPrice = $priceConverter->convert($price);     //product price converted
            $productURL = $this->checkURL($url);                  //product url
            $productID = $id;                                     //product id

    				return '<li class="li_product product col-xs-12 col-sm-4 col-lg-3" >
    					        <div class="thumbnail" style="padding:10px; margin-bottom:10px;">
    				  	          <img src="'. $productImg .'" alt="'. $productName .'" >
    	  	                <div class="caption" align="center">
        					             <h4><b>'. $productName .'</b></h3>
                				       <p class="flex-text">'. $productDescr .'</p>
                                 <h5><b>Miglior Prezzo: '.$productPrice.'</b></h5>
                  				       <div style="margin:0px;">
                                    <a href="'. $productURL .'" class="btn btn-warning btnMarginFix" role="button" >Acquista</a>
                                    <a href="productpage.php?id='. $productID .'" class="btn btn-default btnMarginFix" role="button">Scopri di pi&ugrave;</a>
                                 </div>
                          </div>
                       </div>
                  </li>';
        }

        /**
         * showCategoryLabel
         *
         * Show a Blue label with category name
         *
         * @param string $text category name
         */
        function showCategoryLabel($text){
          echo '<span class="label label-primary" style="margin-right:5px">'.$text.'</span>';
        }

        /**
         * showDesignerLabel
         *
         * Show a Green label with designer name
         *
         * @param string $text designer name
         */
        function showDesignerLabel($text){
          echo '<span class="label label-success" style="margin-right:5px">'.$text.'</span>';
        }

        /**
         * showPublisherLabel
         *
         * Show a Purple label with publisher name
         *
         * @param string $text publisher name
         */
        function showPublisherLabel($text){
          echo '<span class="label label-info" style="margin-right:5px">'.$text.'</span>';
        }
      }

 ?>
