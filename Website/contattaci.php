<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contattaci - Gamair</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">

	</head>
  <body>

    <nav class="navbar navbar-default" style="margin-bottom: 0;" >
        <div class="container-fluid">
          <!-- logo -->
          <a class="navbar-brand" href="index.php" style="padding-top: 18px; padding-right:10px">
              <img src="images/logogamair.gif" width="30" height="30" alt="">
          </a>

          <!-- mobile version -->
    			<div class="navbar-header">
    			  	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
    			    	<span class="sr-only">Toggle navigation</span>
    			    	<span class="icon-bar"></span>
    			    	<span class="icon-bar"></span>
    			    	<span class="icon-bar"></span>
    			  	</button>
    			  	<a class="navbar-brand" href="index.php">GAMAIR</a>
    			</div>

          <!-- Pages links -->
    			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    			  	<ul class="nav navbar-nav">
    			    	<li><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
    			    	<li class="active"><a href="contattaci.php">Contattaci</a></li>
    			  	</ul>

              <!-- Search input -->
    			  	<ul class="nav navbar-nav navbar-right" >
              <form action="index.php" name="search" method="GET" class="navbar-form navbar-left" style="margin:10px 0px 0px 0px;">
    						<div class="form-group">
    						  	<div id="custom-search-input">
    				                <div class="input-group col-md-12">
    				                    <input id="inputSearch" name="search" type="text" class="form-control input-lg" placeholder="Cerca..." />
    				                    <span class="input-group-btn">
    				                        <button class="btn btn-info btn-lg" type="submit" style="background-color: orange">
    				                            <span class="glyphicon glyphicon-search"></span>
    				                        </button>
    				                    </span>
    				                </div>
    				        </div>
    						</div>
    				  </form>
    		     	</ul>
    			</div><!-- navbar -->
	  	</div><!-- container fluid-->
	</nav>

  <!-- Jumbotron slider -->
	<div class="jumbotron" style="margin:0px; padding:50px; background-image: url('images/background.jpg')">
	    <h1>Scopri ora le novit&agrave;</h1>
	    <p>I migliori giochi da tavolo, di carte e di societ&agrave; da tutto il mondo, al miglior prezzo</p>
	    <p><a class="btn btn-warning btn-lg" href="landing.php" role="button">Scopri di pi&ugrave;</a></p>
	</div>


  <!-- Panel -->
	<div class="panel panel-warning">
		<div class="panel-heading">
			<div class="row">

			<a href="index.php" class="btn btn-default" role="button" style="margin-left: 10px">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				&nbsp;Torna alla Home
			</a>
			</div>
		</div>

		<div class="panel-body">
        <!-- Form with inputs -->
				<form action="contattaci.php" name="contactform" method="POST">
				  <div class="form-group">
				    <label><b>Nome e Cognome</b></label>
				    <input name="name" type="text" class="form-control" id="InputName" placeholder="Inserisci il tuo nome e cognome">
				  </div>
				  <div class="form-group">
				    <label><b>Email</b></label>
				    <input  name="email" type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Inserisci la tua email">
				    <small id="emailHelp" class="form-text text-muted">Ti chiediamo di inserire il tuo indirizzo email per essere ricontattato in caso di necessit&agrave;</small>
				  </div>

				  <div class="form-group">
				    <label ><b>Messaggio</b></label>
				    <textarea name="message" class="form-control" id="InputMessage" rows="3" placeholder="Descrivi il tuo problema, consiglio o domanda"></textarea>
				  </div>

				  <div class="form-check">
            <label class="form-check-label">
				      <input name="chkEmail" type="checkbox" class="form-check-input" value="yes">
              Acconsento l'invio di messaggi promozionali riguardanti nuovi prodotti
            </label>
            <br>
            <label class="form-check-label">
				      <input name="chkData" type="checkbox" class="form-check-input" value="yes">
				      Acconsento il trattamento dei dati personali confermando di aver letto la <a href="privacy.html">Privacy Policy</a>
				    </label>
				  </div>
				  <button name="submit" type="submit" class="btn btn-primary">INVIA</button>
				</form>

        <?php
           if($_SERVER['REQUEST_METHOD']=='POST'){                      //if there has been a POST request
               require 'include/functions.php';                         //require functions library

               $db = new GamairDB();                                    //create a new Database connection
               if(!$db){
                 //echo $db->lastErrorMsg();
               } else {
                 //echo "Database aperto <br>";
               }

               if($_POST['chkData'] == "yes"){                          //if there is a tick on personal data agreement

                 if($_POST['chkEmail'] == "yes"){                       //if there is a tick on subscribe me to the newsletter
                    $news = new Newsletter();                           //create a new newsletter object
                    $news->subscribe($_POST['email'], $_POST['name']);  //subscribe it to the newsletter
                 }
                 $email_obj = new Email($_POST['email'], $_POST['name'], $_POST['message']);  //send the email
               }else{
                 //if the user hasn't accepted personal data management
                 $alertData = new Alert("<strong>Attenzione!</strong> Accetta il trattamento dei dati personali.", "danger");
               }
           }
        ?>

		  </div>
    </div>

	  <footer align="center" style="padding-bottom:25px">&copy; Copyright 2016<br>ITIS Cuneo Mario Delpozzo</footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
