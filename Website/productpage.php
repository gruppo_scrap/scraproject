<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gamair - I migliori giochi, al miglior prezzo</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/gamair-custom.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="include/slider.js"></script>
	</head>
  <body>


  <nav class="navbar navbar-default" style="margin-bottom: 0;" >
      <div class="container-fluid">
        <a class="navbar-brand" href="index.php" style="padding-top: 18px; padding-right:10px">
            <img src="images/logogamair.gif" width="30" height="30" alt="">
        </a>

      <!-- specification for mobile version (3 horizontal bars) -->
      <div class="navbar-header">
			  	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			    	<span class="sr-only">Toggle navigation</span>
			    	<span class="icon-bar"></span>
			    	<span class="icon-bar"></span>
			    	<span class="icon-bar"></span>
			  	</button>
			  	<a class="navbar-brand" href="index.php">GAMAIR</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <!-- Page links -->
          <ul class="nav navbar-nav">
			    	<li class="active"><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
			    	<li><a href="contattaci.php">Contattaci</a></li>
			  	</ul>

          <!-- Search form -->
			  	<ul class="nav navbar-nav navbar-right" >
            <form action="index.php" name="search" method="GET" class="navbar-form navbar-left" style="margin:10px 0px 0px 0px;">
  						<div class="form-group">
  						  	<div id="custom-search-input">
  				                <div class="input-group col-md-12">
  				                    <input id="inputSearch" name="search" type="text" class="form-control input-lg" placeholder="Cerca..." />
  				                    <span class="input-group-btn">
  				                        <button class="btn btn-info btn-lg" type="submit" style="background-color: orange">
  				                            <i class="glyphicon glyphicon-search"></i>
  				                        </button>
  				                    </span>
  				                </div>
  				        </div>
  						</div>
  				  </form>
		     	</ul>
			</div><!-- navbar -->
	  	</div><!-- container fluid-->
	</nav>

  <!-- Jumbotron slider -->
	<div class="jumbotron" style="margin:0px; padding:50px; background-image: url('images/background.jpg')">
	    <h1>Scopri ora le novit&agrave;</h1>
	    <p>I migliori giochi da tavolo, di carte e di societ&agrave; da tutto il mondo, al miglior prezzo</p>
	    <p><a class="btn btn-warning btn-lg" href="landing.php" role="button">Scopri di pi&ugrave;</a></p>
	</div>


  <!-- Panel with product details -->
	<div class="panel panel-warning"> <!-- "Warning" because is orange -->
		<div class="panel-heading">
			<div class="row">

      <!-- "back to home" button -->
			<a href="index.php" class="btn btn-default" role="button" style="margin-left: 10px">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				&nbsp;Torna alla Home
			</a>
			</div>
		</div>
		<div class="panel-body">
<?php
      require 'include/functions.php';                  //require functions.php library with all the classes needed
      $db = new GamairDB();                             //new database connection
      /*if(!$db){
        echo $db->lastErrorMsg();
      } else {
        echo "Database aperto <br>";
      }*/
      $rev = new Review();                              //new Review Manager

	    if(isset($_GET["id"])){                           //if id is set
          $id = $_GET["id"];

          //select category IDs
          $queryCategoria =<<<EOD
            SELECT Category_ID
            FROM ProductsCategories
            WHERE Product_ID='$id';
EOD;
          $prodCategories = $db->query($queryCategoria);    //execute query

          //select designers IDs
          $queryDesigners =<<<EOD
    		  	SELECT *
            FROM ProductsDesigners
            WHERE Product_ID='$id';
EOD;
          $prodAuthors = $db->query($queryDesigners);       //execute query

          //select publishers IDs
          $queryPublishers =<<<EOD
            SELECT Publisher_ID
            FROM ProductsPublishers
            WHERE Product_ID='$id';
EOD;
          $prodPublishers = $db->query($queryPublishers);    //execute query

          //select alternative images URLs
          $queryAltImages =<<<EOD
            SELECT Image_Url
            FROM AlternativeImages
            WHERE Product_ID='$id';
EOD;
          $prodAltImages = $db->query($queryAltImages);       //execute query

          //select product details
          $queryDettagli =<<<EOD
            SELECT *
            FROM Products
            WHERE Product_ID='$id';
EOD;
    	   	$retDettagli = $db->query($queryDettagli);            //execute query
    	   	$product = $retDettagli->fetchArray(SQLITE3_ASSOC);   //fetch results

          //verify if there is a product with that ID
          if(!empty($product)){                                 //if there is a product
            $productPage = new ProductManager();                //create new ProductManager object

            $productPage->incrementHits($id, $db);              //increment popularity by incrementing hits

            //creating variables with product details
            $price = new Price();
            $prodPrice = $price->convert($product['Price']);
            $prodName = $product['Name'];
            $prodImageURL = $product['Image_Url'];
            $prodURL = $product['Product_Url'];
            $prodDescr = $product['Description'];
            $prodMinAge = $product['MinAge'];
            $prodMinPlayers = $product['MinPlayers'];
            $prodMaxPlayers = $product['MaxPlayers'];
            $prodYear = $product['Production_Year'];
            $prodMediumRating = round($rev->getMediumRating($id, $db));

            //Checking if there are errors...
            if ($prodName==""){
              $prodName = "Nome del prodotto non disponibile";
            }
            if($prodImageURL==""){
              $prodImageURL = 'images/image_error.png';
            }
            if($prodURL==""){
              $prodURL = "productpage.php?URL=".$prodURL;
            }
            if($prodDescr==""){
              $prodDescr = "Descrizione non diponibile";
            }
            if($prodMinAge==""){
              $prodMinAge = "Sconosciuto";
            }
            if($prodMinPlayers==""){
              $prodMinPlayers = "Sconosciuto";
            }
            if($prodMaxPlayers==-1){
              $prodMaxPlayers = 'Non definito';
            }
            if($prodYear==""){
              $prodYear = "Sconosciuto";
            }

            //left side: images
            echo '<div class="container">
      						  <div class="row">
      						   <div class="col-md-6" style="padding: 0px;">';
                 echo "<div id='carousel-custom' class='carousel slide' data-ride='carousel'>
                        <div class='carousel-outer'>
                            <div class='carousel-inner '>";
                          //First image
                          echo "<div class='item active'>
                                    <img src='".$prodImageURL."'
                                    alt='".$prodName."'/>
                                </div>";
             //alternative images
             while($altImageRow = $prodAltImages->fetchArray(SQLITE3_ASSOC)){
               $altImage = $altImageRow['Image_Url'];
               echo "<div class='item'>
                         <img src='".$altImage."'
                         alt='".$prodName."'/>
                     </div>";
             }
                //script to run the plugin
                echo '<script>
                        $("#zoom_05").elevateZoom({ zoomType    : "inner", cursor: "crosshair" });
                      </script>
                    </div>';  //closed carousel inner

                //photo slider buttons < and > to move between images
                echo "<a class='left carousel-control' href='#carousel-custom' data-slide='prev'>
                            <span class='glyphicon glyphicon-chevron-left'></span>
                        </a>
                        <a class='right carousel-control' href='#carousel-custom' data-slide='next'>
                            <span class='glyphicon glyphicon-chevron-right'></span>
                        </a>
                    </div>";    //closed carousel-outer

                //list of alternative images
                echo "<ol class='carousel-indicators mCustomScrollbar meartlab'>
                          <li data-target='#carousel-custom' data-slide-to='0' class='active'><img src='".$prodImageURL."' alt='' /></li>";
                          $count = 0;
                          while($altImageRow = $prodAltImages->fetchArray(SQLITE3_ASSOC)){
                            $count = $count + 1;
                            $altImage = $altImageRow['Image_Url'];
                            echo '<li data-target="#carousel-custom" data-slide-to="'.$count.'"><img src="'.$altImage.'" alt="'.$prodName.'" /></li>';
                          }

                echo   '</ol>
                       </div>
                     </div>
                   <div class="col-md-6">
        							<h1 class="ProductTitle">'. $prodName .'</h1>
        							<div class="row">
        								<div class="col-md-12">';


              //First PUBLISHER
              if(!$publisherRow = $prodPublishers->fetchArray(SQLITE3_ASSOC)){          //fetch publishers list of that product
                $productPage->showDesignerLabel('Produttore Sconosciuto');              //if it is not available
              }else{
                $publisher = trim($publisherRow['Publisher_ID']);
                $productPage->showPublisherLabel($productPage->idToPub($publisher));    //convert Publisher_ID to Publisher name
              }
              //Other PUBLISHERS
              while($publisherRow = $prodPublishers->fetchArray(SQLITE3_ASSOC)){        //fetch publishers list of that product
                $publisher = trim($publisherRow['Publisher_ID']);
                if ($publisher==""){                                                    //if it is not available
                  $publisher = -1;
                }
                $productPage->showPublisherLabel($productPage->idToPub($publisher));    //convert Publisher_ID to Publisher name
              }

              echo "<br>";


              //First DESIGNER
              if(!$autori = $prodAuthors->fetchArray(SQLITE3_ASSOC)){                   //fetch designers list
                $productPage->showDesignerLabel('Designer Sconosciuto');                //if designer is not available
              }else{
                $autore = trim($autori['Designer_ID']);
                $productPage->showDesignerLabel($productPage->idToDes($autore));        //convert designer id to name
              }
              //Other DESIGNERS
              while($autori = $prodAuthors->fetchArray(SQLITE3_ASSOC)){                 //fetch designers list
                $autore = trim($autori['Designer_ID']);
                if ($autore==""){
                  $autore = -1;                                                         //if designer is not available
                }
                $productPage->showDesignerLabel($productPage->idToDes($autore));        //convert designer id to name
              }

              echo "<br>";

              //First CATEGORY
              if(!$categories = $prodCategories->fetchArray(SQLITE3_ASSOC)){            //fetch categories list
                $productPage->showCategoryLabel("Categoria Sconosciuta");               //if category is not available
              }else{
                $categoria = trim($categories['Category_ID']);
                $productPage->showCategoryLabel($productPage->idToCat($categoria));     //convert id to name
              }
              //Other CATEGORIES
              while($categories = $prodCategories->fetchArray(SQLITE3_ASSOC)){          //fetch categories list
                $categoria = trim($categories['Category_ID']);                          //if category is not available
                if ($categoria==""){
                  $categoria = -1;
                }
                $productPage->showCategoryLabel($productPage->idToCat($categoria));     //convert id to name
              }


      				echo				'</div>
      							</div><!-- end row -->';

      			  echo '<div class="row">
      							 <div class="col-md-3" >';
      				new StarsGroup($prodMediumRating);    //print 5 stars filled or not depending on $prodMediumRating
      				echo '  <span class="label label-warning">';
              echo $rev->getReviewsNumber($id, $db);    //print reviews number in an orange label
              echo '  </span>
      							 </div>
      							</div><!-- end row -->';
      				echo '<div class="row">
      								<div class="col-md-12 bottom-rule">
      								  	<h2 class="product-price">'. $prodPrice . '</h2>
      								</div>
      							 	<a href="'.$prodURL.'" class="btn btn-lg btn-warning btn-full-width" style="margin-left: 17px">
      							   		Acquista ora
      							 	</a>
      							</div><!-- end row -->';

      				echo '</div><!-- end row -->
      							 </div><!-- end container -->
      							 </div>
      			 				<div> <!-- start Nav tabs -->';

              //print nav tabs links
      				echo '<ul class="nav nav-tabs" role="tablist" style="margin-left:20px; position: relative;">
      							 <li role="presentation" class="active">
      							  <a href="#descrizione"
      							   aria-controls="descrizione"
      							   role="tab"
      							   data-toggle="tab"
      							  >Descrizione</a>
      							 </li>
      							 <li role="presentation">
      							  <a href="#dettagli"
      							   aria-controls="dettagli"
      							   role="tab"
      							   data-toggle="tab"
      							  >Dettagli tecnici</a>
      							 </li>
      							</ul>';

              //Tab panes with details (description, minage, players, year of production, etc...)
      				echo '<!-- Tab panes -->
      							<div class="tab-content">
      							 <div role="tabpanel" class="tab-pane active" id="descrizione" style="padding: 10px 20px 10px 20px;">
      							  '. $prodDescr .'
      							 </div>
      							 <div role="tabpanel" class="tab-pane top-10" id="dettagli" style="padding: 10px 20px 10px 20px;">
      							  <table class="table">
      								  <tbody>
      								    <tr>
      								      <td>Et&agrave; minima</td>
      								      <td>'. $prodMinAge .'</td>
      								    </tr>
      								    <tr>
      								      <td>Numero giocatori minimi</td>
      								      <td>'. $prodMinPlayers .'</td>
      								    </tr>
      								    <tr>
      								      <td>Numero giocatori massimi</td>
      								      <td>'. $prodMaxPlayers .'</td>
      								    </tr>
      								    <tr>
      								      <td>Anno di produzione</td>
      								      <td>'. $prodYear .'</td>
      								    </tr>

      								  </tbody>
      								</table>
      							 </div>';


                 if($_SERVER['REQUEST_METHOD']=='POST'){                     //if there has been a POST request
                     $rating=$_POST['ratingValue'];
                     $comments=$_POST['comments'];
                     $name = $_POST['name'];
                     if (is_numeric($rating)&&$rating>=0 && $rating<=5) {     //if rating is numeric and is into the range
                        if(!$name){                                           //if the name is null, set as "Ignoto"
                          $name="Ignoto";
                        }
                     }else{
                       //if the user doesn't select the rating print an alert (is impossible because rating is selectable with a radio button that is selected by default)
                       new Alert("Attenzione, seleziona una valutazione","danger");
                     }
                     $rev->addReview($rating, $comments, $name, $id, $db);    //add the review
                 }else{
                   //if the users hasn't sent a post request, let him do it
                   //print collapse form for reviews
                   echo '<div class="panel-group pnl-recensioni">
                           <div class="panel panel-warning">
                             <div class="panel-heading">
                               <h4 class="panel-title">
                                 <a data-toggle="collapse" href="#valuta" style="text-decoration: none; font-weight:bold;">
                                     <i class="glyphicon glyphicon glyphicon-star"></i>
                                     &nbsp;&nbsp;Valuta prodotto
                                 </a>
                               </h4>
                             </div>';

                   echo '<div id="valuta" class="panel-collapse collapse">
                                <ul class="list-group">
                                  <li class="list-group-item">
                                    <form accept-charset="UTF-8" action="productpage.php?id='.$id.'" method="POST">
                                      <div class="modal-body row">
                                         <div class="col-md-2" style="text-align: center;">
                                                 <div class="radio">
                                                   <label><input type="radio" name="ratingValue" value="0">';
                                                     new StarsGroup(0);
                                             echo '</label>
                                                 </div>
                                                 <div class="radio">
                                                   <label><input type="radio" name="ratingValue" value="1">';
                                                     new StarsGroup(1);
                                             echo '</label>
                                                 </div>
                                                 <div class="radio">
                                                   <label><input type="radio" name="ratingValue" value="2">';
                                                     new StarsGroup(2);
                                             echo '</label>
                                                 </div>
                                                 <div class="radio">
                                                   <label><input type="radio" name="ratingValue" value="3">';
                                                     new StarsGroup(3);
                                             echo '</label>
                                                 </div>
                                                 <div class="radio">
                                                   <label><input type="radio" name="ratingValue" value="4">';
                                                     new StarsGroup(4);
                                             echo '</label>
                                                 </div>
                                                 <div class="radio">
                                                   <label><input checked="checked" type="radio" name="ratingValue" value="5">';
                                                     new StarsGroup(5);
                                             echo '</label>
                                                 </div>

                                         </div>

                                         <div class="col-md-10" style="text-align:right;">
                                             <input type="text" class="form-control" name="name" placeholder="Inserisci il tuo nome...">
                                             <textarea class="form-control" rows="5" name="comments" placeholder="Inserisci un commento..."></textarea>
                                             <button class="btn btn-warning" type="submit" style="margin-top:10px"><b>Invia valutazione</b></button>
                                         </div>
                                      </div>
                                    </form>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>';
                 }

                  echo '<div class="panel-group pnl-recensioni">
                          <div class="panel panel-warning">
                            <div class="panel-heading">
                              <h4 class="panel-title">
                                <a data-toggle="collapse" href="#recensioni" style="text-decoration: none; font-weight:bold;">
                                    <i class="glyphicon glyphicon glyphicon-thumbs-up"></i>
                                    &nbsp;&nbsp;Visualizza recensioni
                                </a>
                              </h4>
                            </div>
                            <div id="recensioni" class="panel-collapse collapse">
                              <ul class="list-group">';

                              $rev->printReviews($id);

               echo     '     </ul>
                            </div>
                          </div>
                        </div>';
          }else{
            //if product id is set, but there isn't into the database print alert
            new Alert('<div align="center"><b>Il prodotto che stai cercando non è presente nella nostra raccolta</b></div>',"danger ");
          }

	    }else{
        //if id is not set in the GET call
        new Alert('<div align="center"><b>Il prodotto che stai cercando non è presente nella nostra raccolta</b></div>',"danger ");
      }
      $db->close();
?>


          </div>
    		</div>
    	</div>
    </div>

    <footer align="center" style="padding-bottom:25px">&copy; Copyright 2016<br>ITIS Cuneo Mario Delpozzo</footer>

    <!-- script for alternative images -->
    <script>$(document).ready(function() {
        $(".mCustomScrollbar").mCustomScrollbar({axis:"x"});
    })</script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
