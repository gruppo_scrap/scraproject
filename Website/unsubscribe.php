<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Newsletter - Gamair</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
	</head>

  <body>
    <nav class="navbar navbar-default" style="margin-bottom: 0;" >
        <div class="container-fluid">
          <!-- logo -->
          <a class="navbar-brand" href="index.php" style="padding-top: 18px; padding-right:10px">
              <img src="images/logogamair.gif" width="30" height="30" alt="">
          </a>

          <!-- icon bars for mobile -->
    			<div class="navbar-header">
    			  	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
    			    	<span class="sr-only">Toggle navigation</span>
    			    	<span class="icon-bar"></span>
    			    	<span class="icon-bar"></span>
    			    	<span class="icon-bar"></span>
    			  	</button>
    			  	<a class="navbar-brand" href="index.php">GAMAIR</a>
    			</div>

    			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <!-- links -->
              <ul class="nav navbar-nav">
    			    	<li><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
    			    	<li class="active"><a href="contattaci.php">Contattaci</a></li>
    			  	</ul>

              <!-- search input -->
    			  	<ul class="nav navbar-nav navbar-right" >
    					<form class="navbar-form navbar-left" style="margin:10px 0px 0px 0px;">
    						<div class="form-group">
    						  	<div id="custom-search-input">
    				                <div class="input-group col-md-12">
    				                    <input type="text" class="form-control input-lg" placeholder="Cerca..." />
    				                    <span class="input-group-btn">
    				                        <button class="btn btn-info btn-lg" type="button" style="background-color: orange">
    				                            <i class="glyphicon glyphicon-search"></i>
    				                        </button>
    				                    </span>
    				                </div>
    				            </div>
    						</div>
    				  </form>
    		     	</ul>
    			</div><!-- navbar -->
	  	</div><!-- container fluid-->
	</nav>

  <!-- jumbotron slider -->
	<div class="jumbotron" style="margin:0px; padding:50px; background-image: url('images/background.jpg')">
	    <h1>Scopri ora le novit&agrave;</h1>
	    <p>I migliori giochi da tavolo, di carte e di societ&agrave; da tutto il mondo, al miglior prezzo</p>
	    <p><a class="btn btn-warning btn-lg" href="landing.php" role="button">Scopri di pi&ugrave;</a></p>
	</div>


  <!-- panel -->
	<div class="panel panel-warning">
		<div class="panel-heading">
			<div class="row">
			<a href="index.php" class="btn btn-default" role="button" style="margin-left: 10px">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				&nbsp;Torna alla Home
			</a>
			</div>
		</div>

		<div class="panel-body">
<?php
        require 'include/functions.php';                  //require functions.php library

        if($_SERVER['REQUEST_METHOD']=='POST'){           //if there has been a POST request
          if(isset($_POST['email'])){                     //if email is set
            $email = $_POST['email'];
            $newsletter = new Newsletter();               //create a new newsletter object
            $newsletter->unsubscribe($email);             //unsubscribe from the newsletter
          }else{                                          //if it goes wrong, show alert
            $alertDataPost = new Alert("<strong>Attenzione!</strong> Dati non ricevuti", "danger");
          }
        }else{                                            //if there has been a GET request
          if(isset($_GET['email'])){                      //if isset email
            $email = $_GET['email'];
            echo '<form method="POST" action="unsubscribe.php">
                    <div style="text-align: center;">
                      <img src="images/user.png" width="150px">
                      <br>';                              //print unsubscribe form with a general profile image
            $news= new Newsletter();                      //create a new Newsletter object
            $name = $news->getName($email);               //get the name from newsletter object
            if(trim($name)==""){                          //is a valid name?
              $name=$email;
            }
            echo '<h4> Ciao '. $name .'</h4>';
            echo '<h5>Confermi di voler disdire la tua iscrizione alla newsletter di Gamair?</h5>
                      <button name="submit" type="submit" class="btn btn-warning">CONFERMA</button>
                      <input type="hidden" name="email" id="hiddenField" value="'.$email.'">
                    </div>
                  </form>';
          }else{
            //if the page doesn't receive data, print an alert
            $alertData = new Alert("<strong>Attenzione!</strong> Dati non ricevuti", "danger");
          }
        }
?>
		  </div>
    </div>  <!-- Panel closed -->

	  <footer align="center" style="padding-bottom:25px">&copy; Copyright 2016<br>ITIS Cuneo Mario Delpozzo</footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
